package com.social.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class EmailSender {

    @Autowired
    private JavaMailSender javaMailSender;

    @Value("${spring.mail.username}")
    private String email;

    @Async
    public void sendRegistrationToken(String toEmail, String token) {
        System.out.println(email);
        System.out.println("");
        System.out.println("");
        System.out.println("");
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = null;

        try {
            helper = new MimeMessageHelper(message,true);
            helper.setSubject("Confirmation email");
            helper.setTo(toEmail);
            String body = "https://77.81.178.198:25001/social/registration/" + token;
            helper.setText(body,true);

        } catch (MessagingException e) {
            e.printStackTrace();
        }

        javaMailSender.send(message);
    }
}
