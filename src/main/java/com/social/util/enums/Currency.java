package com.social.util.enums;

public enum Currency {
    RON, EUR, USD, GBP, CHF
}
