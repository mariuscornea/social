package com.social.util.enums;

public enum OfferType {

    SMALL, MEDIUM, LONG
}
