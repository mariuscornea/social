package com.social.util;

import com.vividsolutions.jts.geom.*;

public class GeomUtil {

    private GeomUtil() {
    }

    public static Geometry latLongToGeom(Double latitude, Double longitude) {
        GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326);
        Point point = geometryFactory.createPoint(new Coordinate(longitude, latitude));
        return point;
    }
}
