package com.social.util.date;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class DateUtil {

    public static Long localDateTimeToLong(LocalDateTime localDateTime) {
        return localDateTime.toInstant(ZoneOffset.UTC).toEpochMilli();
    }
}
