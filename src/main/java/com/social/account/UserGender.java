package com.social.account;

public enum UserGender {
    MALE, FEMALE;
}
