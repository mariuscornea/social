package com.social.account.dto;

import lombok.Data;

@Data
public class AccountPicturesDTO {

    private String coverPicture;
    private String profilePicture;
}
