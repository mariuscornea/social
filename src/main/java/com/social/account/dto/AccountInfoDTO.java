package com.social.account.dto;

import com.social.account.UserGender;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class AccountInfoDTO {

    @NotNull
    private UserGender gender;

    @NotNull
    private Boolean hasBusiness;

    @NotNull
    private Boolean isExpert;

    @NotNull
    private String phoneNumber;

    @Valid
    @NotNull
    private BirthDTO birth;

    @Min(-90)
    @Max(90)
    @NotNull
    private Double latitude;

    @Min(-180)
    @Max(180)
    @NotNull
    private Double longitude;
}
