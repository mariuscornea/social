package com.social.account.dto;

import com.social.account.UserGender;
import lombok.Data;

@Data
public class AccountDTO {

    private Integer id;

    private UserGender gender;

    private Boolean hasBusiness;

    private Boolean isExpert;

    private String phoneNumber;

    private BirthDTO birth;

    private Double latitude;

    private Double longitude;
}
