package com.social.account.dto;

import lombok.Data;

@Data
public class ProfilePhotoResponse {

    private Integer id;
    private String photoSrc;
}
