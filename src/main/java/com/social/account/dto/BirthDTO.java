package com.social.account.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class BirthDTO {

    @Min(1)
    @Max(31)
    @NotNull
    private Integer day;

    @Min(1)
    @Max(12)
    @NotNull
    private Integer month;

    @Min(1908)
    @Max(2018)
    @NotNull
    private Integer year;
}
