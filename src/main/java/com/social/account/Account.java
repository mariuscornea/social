package com.social.account;

import com.vividsolutions.jts.geom.Geometry;
import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@Table(name = "accounts",
        indexes = {
                @Index(name = "email_index", columnList = "email", unique = true)
        })
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @Column
    private Boolean active;

    @Column
    private Boolean hasBusiness;

    @Column
    private Boolean isExpert;

    @Column
    private Boolean firstLogin;

    @Column
    private String photoSrc;

    @Column
    private String coverPicture;

    @Column
    private String firstName;

    @Column
    private String lastName;

    @Column
    private String email;

    @Column(nullable = false, length = 60)
    private String password;

    @Enumerated(EnumType.STRING)
    private UserRole userRole;

    @Enumerated(EnumType.STRING)
    private UserGender userGender;

    @Column(columnDefinition = "geometry(Point,4326)")
    private Geometry geometry;

    private Integer day;

    private Integer month;

    private Integer year;

    private String phoneNumber;

    private Double latitude;

    private Double longitude;

    @Column(columnDefinition = "uuid")
    private UUID uuid = UUID.randomUUID();
}
