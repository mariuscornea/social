package com.social.account;

import com.social.ResponseWithMessage;
import com.social.account.dto.AccountInfoDTO;
import com.social.account.dto.AccountPicturesDTO;
import com.social.account.dto.ProfilePhotoResponse;
import com.social.core.exception.FieldsException;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/auth")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/accounts/add-profile-picture", method = RequestMethod.POST)
    public ResponseWithMessage<ProfilePhotoResponse> addProfilePicture(@AuthenticationPrincipal UserContext userContext,
                                                                       @RequestParam(name = "file") MultipartFile file) {
        return new ResponseWithMessage(accountService.addProfilePicture(userContext, file), "Success message");
    }

    @RequestMapping(value = "/accounts/get-profile-picture", method = RequestMethod.GET)
    public ProfilePhotoResponse getProfilePicture(@AuthenticationPrincipal UserContext userContext) {
        return accountService.getProfilePicture(userContext);
    }

    @RequestMapping(value = "/accounts/add-profile-info", method = RequestMethod.POST)
    public ResponseWithMessage<Void> addProfileInfo(@AuthenticationPrincipal UserContext userContext,
                                                    @Valid @RequestBody AccountInfoDTO accountInfoDTO,
                                                    BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Account fields are incorrect", "msg.key", result);
        }
        accountService.addProfileInfo(userContext, accountInfoDTO);
        return new ResponseWithMessage("Success message");
    }

    @RequestMapping(value = "/accounts/photos", method = RequestMethod.GET)
    public AccountPicturesDTO getAccountPictures(@AuthenticationPrincipal UserContext userContext) {
        return accountService.getAccountPicturesDTO(userContext);
    }
}
