package com.social.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class AccountPublicController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/accounts/check-email/{email}", method = RequestMethod.GET)
    public boolean checkIfEmailExists(@PathVariable("email") String email) {
        return accountService.checkIfAccountExists(email);
    }

}
