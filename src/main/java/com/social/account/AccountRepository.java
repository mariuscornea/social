package com.social.account;


import com.social.account.projection.AccountPictures;
import com.social.core.repo.BaseJpaRepository;

import java.util.Optional;

public interface AccountRepository extends BaseJpaRepository<Account, Integer> {

    Optional<Account> findByEmail(
            String email);

    Account findAccountByEmail(
            String email);

    AccountPictures findProjById(Integer id);
}
