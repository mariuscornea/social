package com.social.account.miscellaneous;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MiscellaneousDTO {

    private List<String> hobbies = new ArrayList<>();
    private List<String> interests = new ArrayList<>();
    private List<String> professions = new ArrayList<>();
}
