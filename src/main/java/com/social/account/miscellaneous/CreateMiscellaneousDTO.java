package com.social.account.miscellaneous;

import com.social.core.annotation.NotEmptyList;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CreateMiscellaneousDTO {

    @NotEmptyList
    private List<String> hobbies = new ArrayList<>();

    @NotEmptyList
    private List<String> interests = new ArrayList<>();

    @NotEmptyList
    private List<String> professions = new ArrayList<>();
}
