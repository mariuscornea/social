package com.social.account.miscellaneous.profession;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountProfessionRepository extends JpaRepository<AccountProfession, Integer>{

    AccountProfession findByAccountIdAndProfessionId(Integer accountId, Integer professionId);
}
