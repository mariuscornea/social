package com.social.account.miscellaneous.profession;


import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfessionRepository extends JpaRepository<Profession, Integer>{

    Profession findByName(String name);
}
