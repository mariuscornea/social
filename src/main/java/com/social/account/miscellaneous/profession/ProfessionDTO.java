package com.social.account.miscellaneous.profession;

import lombok.Data;

@Data
public class ProfessionDTO {

    private String name;
}
