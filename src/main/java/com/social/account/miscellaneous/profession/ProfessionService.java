package com.social.account.miscellaneous.profession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProfessionService {

    @Autowired
    private ProfessionRepository professionRepository;

    public Profession addProfessionOrGetProfession(String professionName){
        Profession profession = professionRepository.findByName(professionName);
        if(profession == null){
            profession = new Profession();
            profession.setName(professionName);
            professionRepository.save(profession);
        }
        return profession;
    }
}
