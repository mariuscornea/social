package com.social.account.miscellaneous.profession;

import com.social.account.Account;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class AccountProfession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Account account;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Profession profession;
}
