package com.social.account.miscellaneous;

import com.social.account.Account;
import com.social.account.AccountRepository;
import com.social.account.miscellaneous.hobby.AccountHobby;
import com.social.account.miscellaneous.hobby.AccountHobbyRepository;
import com.social.account.miscellaneous.hobby.Hobby;
import com.social.account.miscellaneous.hobby.HobbyRepository;
import com.social.account.miscellaneous.interest.AccountInterest;
import com.social.account.miscellaneous.interest.AccountInterestRepository;
import com.social.account.miscellaneous.interest.Interest;
import com.social.account.miscellaneous.interest.InterestRepository;
import com.social.account.miscellaneous.profession.AccountProfession;
import com.social.account.miscellaneous.profession.AccountProfessionRepository;
import com.social.account.miscellaneous.profession.Profession;
import com.social.account.miscellaneous.profession.ProfessionRepository;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

@Service
@Transactional
public class MiscellaneousService {

    @Autowired
    private HobbyRepository hobbyRepository;

    @Autowired
    private AccountHobbyRepository accountHobbyRepository;

    @Autowired
    private InterestRepository interestRepository;

    @Autowired
    private AccountInterestRepository accountInterestRepository;

    @Autowired
    private ProfessionRepository professionRepository;

    @Autowired
    private AccountProfessionRepository accountProfessionRepository;

    @Autowired
    private AccountRepository accountRepository;

    public MiscellaneousDTO getAllMisc() {
        MiscellaneousDTO miscellaneousDTO = new MiscellaneousDTO();
        miscellaneousDTO.setHobbies(hobbyRepository.findAll().stream().map(hobby -> hobby.getName()).collect(Collectors.toList()));
        miscellaneousDTO.setInterests(interestRepository.findAll().stream().map(interest -> interest.getName()).collect(Collectors.toList()));
        miscellaneousDTO.setProfessions(professionRepository.findAll().stream().map(profession -> profession.getName()).collect(Collectors.toList()));
        return miscellaneousDTO;
    }

    public void saveMisc(UserContext userContext, CreateMiscellaneousDTO createMiscellaneousDTO) {
        Account account = accountRepository.findOne(userContext.getId());
        addHobbies(account, createMiscellaneousDTO);
        addInterest(account, createMiscellaneousDTO);
        addProfession(account, createMiscellaneousDTO);
        account.setFirstLogin(false);
    }

    private void addHobbies(Account account, CreateMiscellaneousDTO createMiscellaneousDTO) {
        createMiscellaneousDTO.getHobbies().forEach(hobbyName -> {
            Hobby hobby = hobbyRepository.findByName(hobbyName);
            if (hobby == null) {
                hobby = new Hobby();
                hobby.setName(hobbyName);
                hobbyRepository.save(hobby);
            }

            AccountHobby accountHobby = accountHobbyRepository.findByAccountIdAndHobbyId(account.getId(), hobby.getId());
            if (accountHobby == null) {
                accountHobby = new AccountHobby();
                accountHobby.setAccount(account);
                accountHobby.setHobby(hobby);
                accountHobbyRepository.save(accountHobby);
            }

        });
    }

    private void addInterest(Account account, CreateMiscellaneousDTO createMiscellaneousDTO) {
        createMiscellaneousDTO.getInterests().forEach(interestName -> {
            Interest interest = interestRepository.findByName(interestName);
            if (interest == null) {
                interest = new Interest();
                interest.setName(interestName);
                interestRepository.save(interest);
            }
            AccountInterest accountInterest = accountInterestRepository.findByAccountIdAndInterestId(account.getId(), interest.getId());
            if (accountInterest == null) {
                accountInterest = new AccountInterest();
                accountInterest.setAccount(account);
                accountInterest.setInterest(interest);
                accountInterestRepository.save(accountInterest);
            }
        });
    }

    private void addProfession(Account account, CreateMiscellaneousDTO createMiscellaneousDTO) {
        createMiscellaneousDTO.getProfessions().forEach(professionName -> {
            Profession profession = professionRepository.findByName(professionName);
            if (profession == null) {
                profession = new Profession();
                profession.setName(professionName);
                professionRepository.save(profession);
            }

            AccountProfession accountProfession = accountProfessionRepository.findByAccountIdAndProfessionId(account.getId(), profession.getId());
            if (accountProfession == null) {
                accountProfession = new AccountProfession();
                accountProfession.setAccount(account);
                accountProfession.setProfession(profession);
                accountProfessionRepository.save(accountProfession);
            }
        });
    }

}
