package com.social.account.miscellaneous;

import com.social.ResponseWithMessage;
import com.social.core.exception.FieldsException;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api")
public class MiscellaneousController {

    @Autowired
    private MiscellaneousService miscellaneousService;

    @RequestMapping(value = "/accounts/misc", method = RequestMethod.GET)
    public MiscellaneousDTO findAll() {
        return miscellaneousService.getAllMisc();
    }

    @RequestMapping(value = "/auth/accounts/misc", method = RequestMethod.POST)
    public ResponseWithMessage<Void> addMisc(@AuthenticationPrincipal UserContext userContext,
                                             @Valid @RequestBody CreateMiscellaneousDTO createMiscellaneousDTO,
                                             BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Misc fields are incorrect", "msg.key", result);
        }
        miscellaneousService.saveMisc(userContext, createMiscellaneousDTO);

        return new ResponseWithMessage("Success message");
    }
}
