package com.social.account.miscellaneous.interest;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Interest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String name;
}
