package com.social.account.miscellaneous.interest;

import com.social.core.repo.BaseJpaRepository;

public interface InterestRepository extends BaseJpaRepository<Interest, Integer>{

    Interest findByName(String name);
}
