package com.social.account.miscellaneous.interest;

import lombok.Data;

@Data
public class InterestDTO {

    private String name;
}
