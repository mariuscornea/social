package com.social.account.miscellaneous.interest;

import com.social.account.Account;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class AccountInterest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Account account;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Interest interest;
}
