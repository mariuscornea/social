package com.social.account.miscellaneous.interest;

import com.social.core.repo.BaseJpaRepository;

public interface AccountInterestRepository extends BaseJpaRepository<AccountInterest, Integer>{

    AccountInterest findByAccountIdAndInterestId(Integer accountId, Integer interestId);
}
