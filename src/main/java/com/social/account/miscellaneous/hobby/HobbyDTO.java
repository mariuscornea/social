package com.social.account.miscellaneous.hobby;

import lombok.Data;

@Data
public class HobbyDTO {

    private String name;
}
