package com.social.account.miscellaneous.hobby;

import com.social.core.repo.BaseJpaRepository;

public interface HobbyRepository extends BaseJpaRepository<Hobby, Integer>{

    Hobby findByName(String name);

}
