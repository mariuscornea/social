package com.social.account.miscellaneous.hobby;

import com.social.core.repo.BaseJpaRepository;

public interface AccountHobbyRepository extends BaseJpaRepository<AccountHobby, Integer> {

    AccountHobby findByAccountIdAndHobbyId(Integer accountId, Integer hobbyId);
}
