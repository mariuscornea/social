package com.social.account.miscellaneous.hobby;

import com.social.account.Account;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class AccountHobby {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Account account;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Hobby hobby;

}
