package com.social.account.projection;

public interface AccountPictures {

    String getPhotoSrc();

    String getCoverPicture();
}
