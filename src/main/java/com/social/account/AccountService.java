package com.social.account;

import com.social.account.dto.AccountInfoDTO;
import com.social.account.dto.AccountPicturesDTO;
import com.social.account.dto.ProfilePhotoResponse;
import com.social.account.projection.AccountPictures;
import com.social.file.FileService;
import com.social.security.model.UserContext;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.PrecisionModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@Service
@Transactional
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountConverter accountConverter;

    @Autowired
    private FileService fileService;

    public Optional<Account> findByEmail(
            final String email) {
        return this.accountRepository.findByEmail(email);
    }

    public boolean checkIfAccountExists(String email) {
        return accountRepository.findAccountByEmail(email) != null ? true : false;
    }

    public ProfilePhotoResponse addProfilePicture(UserContext userContext, MultipartFile multipartFile) {
        Account account = accountRepository.findOne(userContext.getId());
        String photoSrc = fileService.saveFileToDisk(userContext.getUsername(), multipartFile);
        account.setPhotoSrc(photoSrc);
        accountRepository.save(account);

        ProfilePhotoResponse profilePhotoResponse = new ProfilePhotoResponse();
        profilePhotoResponse.setId(account.getId());
        profilePhotoResponse.setPhotoSrc(account.getPhotoSrc());
        return profilePhotoResponse;
    }

    public ProfilePhotoResponse getProfilePicture(UserContext userContext){
        Account account = accountRepository.findOne(userContext.getId());
        ProfilePhotoResponse profilePhotoResponse = new ProfilePhotoResponse();
        profilePhotoResponse.setId(account.getId());
        profilePhotoResponse.setPhotoSrc(account.getPhotoSrc());
        return profilePhotoResponse;
    }

    public void addProfileInfo(UserContext userContext, AccountInfoDTO accountInfoDTO) {
        Account account = accountRepository.findOne(userContext.getId());
        accountConverter.toEntityInfo(account, accountInfoDTO);

        GeometryFactory geometryFactory = new GeometryFactory(new PrecisionModel(), 4326);
        Point point = geometryFactory.createPoint(new Coordinate(accountInfoDTO.getLatitude(), accountInfoDTO.getLongitude()));
        account.setGeometry(point);

        accountRepository.save(account);
    }

    public AccountPicturesDTO getAccountPicturesDTO(UserContext userContext){
        AccountPictures accountPictures = accountRepository.findProjById(userContext.getId());
        return accountConverter.toAccountPicturesDTO(accountPictures);
    }

}
