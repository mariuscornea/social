package com.social.account;

import com.social.account.dto.AccountInfoDTO;
import com.social.account.dto.AccountPicturesDTO;
import com.social.account.projection.AccountPictures;
import org.springframework.stereotype.Component;

@Component
public class AccountConverter {

    public Account toEntityInfo(Account account, AccountInfoDTO accountInfoDTO) {
        account.setHasBusiness(accountInfoDTO.getHasBusiness());
        account.setIsExpert(accountInfoDTO.getIsExpert());
        account.setDay(accountInfoDTO.getBirth().getDay());
        account.setMonth(accountInfoDTO.getBirth().getMonth());
        account.setYear(accountInfoDTO.getBirth().getYear());
        account.setPhoneNumber(accountInfoDTO.getPhoneNumber());
        account.setUserGender(accountInfoDTO.getGender());
        account.setLongitude(accountInfoDTO.getLongitude());
        account.setLatitude(accountInfoDTO.getLatitude());
        return account;
    }

    public AccountPicturesDTO toAccountPicturesDTO(AccountPictures accountPictures){
        AccountPicturesDTO accountPicturesDTO = new AccountPicturesDTO();
        accountPicturesDTO.setProfilePicture(accountPictures.getPhotoSrc());
        accountPicturesDTO.setCoverPicture(accountPictures.getCoverPicture());

        return accountPicturesDTO;
    }
}
