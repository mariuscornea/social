package com.social.account;

public enum UserRole {
    ADMIN, USER;
}
