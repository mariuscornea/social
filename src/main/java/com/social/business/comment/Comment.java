package com.social.business.comment;

import com.social.account.Account;
import com.social.business.Business;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Account account;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Business business;

    @Column(nullable = false)
    private String comment;

    @Column(nullable = false)
    private LocalDateTime commentCreatedDate = LocalDateTime.now();

    @Column
    private String replyComment;

    @Column(nullable = false)
    private LocalDateTime replyCommentCreatedDate;
}
