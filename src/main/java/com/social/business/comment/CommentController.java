package com.social.business.comment;

import com.social.ResponseWithMessage;
import com.social.business.comment.dto.CommentDTO;
import com.social.business.comment.dto.CreateCommentDTO;
import com.social.business.comment.dto.ReplyCommentDTO;
import com.social.business.dto.BusinessDTO;
import com.social.core.exception.FieldsException;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/auth/businesses/{businessId}/comments", method = RequestMethod.POST)
    public ResponseWithMessage<BusinessDTO> createComment(@AuthenticationPrincipal UserContext userContext,
                                                          @PathVariable("businessId") Integer businessId,
                                                          @Valid @RequestBody CreateCommentDTO createCommentDTO,
                                                          BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Comment fields are incorrect", "msg.key", result);
        }
        commentService.createComment(userContext, businessId, createCommentDTO);
        return new ResponseWithMessage("Comment added with success");
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/comments/{commentId}/replies", method = RequestMethod.POST)
    public ResponseWithMessage<BusinessDTO> replyToComment(@AuthenticationPrincipal UserContext userContext,
                                                           @PathVariable("businessId") Integer businessId,
                                                           @PathVariable("commentId") Integer commentId,
                                                           @Valid @RequestBody ReplyCommentDTO replyCommentDTO,
                                                           BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Comment reply fields are incorrect", "msg.key", result);
        }
        commentService.replyToComment(userContext, businessId, commentId, replyCommentDTO);
        return new ResponseWithMessage("Comment reply added with success");
    }

    @RequestMapping(value = "/businesses/{businessId}/comments", method = RequestMethod.GET)
    public Page<CommentDTO> getCommentsForBusiness(@PathVariable("businessId") Integer businessId,
                                                   @PageableDefault(page = 0, size = 20) Pageable pageable) {
        return commentService.listComments(businessId, pageable);
    }
}
