package com.social.business.comment;

import com.social.business.comment.dto.CommentDTO;
import com.social.business.comment.dto.CreateCommentDTO;
import com.social.business.comment.dto.ReplyCommentDTO;
import com.social.util.date.DateUtil;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class CommentConverter {

    public Comment toEntity(CreateCommentDTO createCommentDTO) {
        Comment comment = new Comment();
        comment.setComment(createCommentDTO.getComment());
        return comment;
    }

    public Comment toEntity(Comment comment, ReplyCommentDTO replyCommentDTO) {
        comment.setComment(replyCommentDTO.getComment());
        comment.setReplyCommentCreatedDate(LocalDateTime.now());
        return comment;
    }

    public CommentDTO toDTO(Comment comment) {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setId(comment.getId());
        commentDTO.setUsername(comment.getAccount().getFirstName() + " " + comment.getAccount().getLastName());
        commentDTO.setComment(comment.getComment());
        commentDTO.setCommentCreatedDate(DateUtil.localDateTimeToLong(comment.getCommentCreatedDate()));
        commentDTO.setReplyComment(comment.getReplyComment());
        commentDTO.setReplyCommentCreatedDate(DateUtil.localDateTimeToLong(comment.getReplyCommentCreatedDate()));

        return commentDTO;
    }

    public Page<CommentDTO> toDTOPage(Page<Comment> comments){
        return comments.map(this::toDTO);
    }
}
