package com.social.business.comment.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ReplyCommentDTO {

    @NotEmpty
    private String comment;
}
