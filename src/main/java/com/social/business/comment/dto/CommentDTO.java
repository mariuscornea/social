package com.social.business.comment.dto;

import lombok.Data;

@Data
public class CommentDTO {

    private Integer id;
    private String username;
    private String comment;
    private Long commentCreatedDate;
    private String replyComment;
    private Long replyCommentCreatedDate;
}
