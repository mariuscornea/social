package com.social.business.comment.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class CreateCommentDTO {

    @NotEmpty
    private String comment;
}
