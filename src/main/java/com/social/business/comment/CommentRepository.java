package com.social.business.comment;

import com.social.core.repo.BaseJpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface CommentRepository extends BaseJpaRepository<Comment, Integer> {

    Optional<Comment> findByIdAndBusinessId(Integer commentId, Integer businessId);

    Comment findByAccountIdAndBusinessId(Integer accountId, Integer businessId);

    Page<Comment> findByBusinessIdOrderByCommentCreatedDateDesc(Integer businessId, Pageable pageable);
}
