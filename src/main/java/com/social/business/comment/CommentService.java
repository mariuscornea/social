package com.social.business.comment;

import com.social.account.AccountRepository;
import com.social.business.Business;
import com.social.business.BusinessRepository;
import com.social.business.comment.dto.CommentDTO;
import com.social.business.comment.dto.CreateCommentDTO;
import com.social.business.comment.dto.ReplyCommentDTO;
import com.social.core.exception.ConflictException;
import com.social.core.exception.EntityNotFoundException;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private CommentConverter commentConverter;

    @Autowired
    private BusinessRepository businessRepository;

    @Autowired
    private AccountRepository accountRepository;

    public CommentDTO createComment(UserContext userContext, Integer businessId, CreateCommentDTO createCommentDTO) {
        //TODO don't let user comment if is his business or dosen't have deal with the business
        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));

        Comment comment = commentRepository.findByAccountIdAndBusinessId(userContext.getId(), businessId);
        if (comment != null) {
            throw new ConflictException("Can't comment multiple times on the same business");
        }
        comment = commentConverter.toEntity(createCommentDTO);
        comment.setAccount(accountRepository.getOne(userContext.getId()));
        comment.setBusiness(business);
        return commentConverter.toDTO(comment);
    }

    public CommentDTO replyToComment(UserContext userContext, Integer businessId, Integer commentId, ReplyCommentDTO replyCommentDTO) {
        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));
        Comment comment = commentRepository.findByIdAndBusinessId(commentId, business.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));
        if (comment.getReplyComment() != null) {
            throw new ConflictException("Can't reply multiple times to same comment");
        }
        commentConverter.toEntity(comment, replyCommentDTO);
        return commentConverter.toDTO(commentRepository.save(comment));
    }

    public Page<CommentDTO> listComments(Integer businessId, Pageable pageable){
        return commentConverter.toDTOPage(commentRepository.findByBusinessIdOrderByCommentCreatedDateDesc(businessId, pageable));
    }
}
