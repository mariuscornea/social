package com.social.business.service;

import com.social.business.service.dto.CreateServicesDTO;
import com.social.business.service.dto.ServicesDTO;
import com.social.tags.benefit.BenefitTagConverter;
import com.social.tags.product.ProductTagConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ServicesConverter {

    @Autowired
    private BenefitTagConverter benefitTagConverter;

    @Autowired
    private ProductTagConverter productTagConverter;

    public ServicesDTO toDTO(Services services) {
        ServicesDTO servicesDTO = new ServicesDTO();
        servicesDTO.setId(services.getId());
        servicesDTO.setName(services.getName());
        servicesDTO.setPhotoSrc(services.getPhotoSrc());
        servicesDTO.setHour(services.getHour());
        servicesDTO.setDescription(services.getDescription());
        servicesDTO.setBenefitsTags(benefitTagConverter.toList(services.getBenefitTags()));
        servicesDTO.setProductTags(productTagConverter.toList(services.getProductTags()));

        return servicesDTO;
    }

    public Services toEntity(CreateServicesDTO createServicesDTO) {
        Services services = new Services();
        services.setName(createServicesDTO.getName());
        services.setHour(createServicesDTO.getHour());
        services.setDescription(createServicesDTO.getDescription());

        return services;
    }

    public Services toEntity(Services services, CreateServicesDTO createServicesDTO) {
        services.setName(createServicesDTO.getName());
        services.setHour(createServicesDTO.getHour());
        services.setDescription(createServicesDTO.getDescription());

        return services;
    }

    public List<ServicesDTO> toDTOList(List<Services> services) {
        return services.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
