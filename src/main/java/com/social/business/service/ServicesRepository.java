package com.social.business.service;

import com.social.core.repo.BaseJpaRepository;

import java.util.List;
import java.util.Optional;

public interface ServicesRepository extends BaseJpaRepository<Services, Integer> {

    List<Services> findByBusinessIdOrderByIdDesc(Integer business);

    Optional<Services> findByIdAndBusinessId(Integer id, Integer business);
}
