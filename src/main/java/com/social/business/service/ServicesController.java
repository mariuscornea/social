package com.social.business.service;

import com.social.ResponseWithMessage;
import com.social.business.service.dto.CreateServicesDTO;
import com.social.business.service.dto.ServicesDTO;
import com.social.core.exception.FieldsException;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class ServicesController {

    @Autowired
    private ServicesService servicesService;

    @RequestMapping(value = "/benefits/tags", method = RequestMethod.GET)
    public List<String> benefitsTags() {
        return servicesService.findBenefitTags();
    }

    @RequestMapping(value = "/products/tags", method = RequestMethod.GET)
    public List<String> productsTags() {
        return servicesService.findProductTags();
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/services", method = RequestMethod.GET)
    public List<ServicesDTO> getServices(@PathVariable("businessId") Integer businessId) {
        return servicesService.findServices(businessId);
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/services/{serviceId}", method = RequestMethod.GET)
    public ServicesDTO getServiceById(@PathVariable("serviceId") Integer serviceId) {
        return servicesService.findServicesById(serviceId);
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/services", method = RequestMethod.POST)
    public ResponseWithMessage<ServicesDTO> createServices(@AuthenticationPrincipal UserContext userContext,
                                                           @PathVariable("businessId") Integer businessId,
                                                           @Valid @RequestBody CreateServicesDTO createServicesDTO,
                                                           BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Service fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage<>(servicesService.createServices(userContext, businessId, createServicesDTO), "Success create service");
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/services/{serviceId}/photos", method = RequestMethod.PUT)
    public ResponseWithMessage<ServicesDTO> addServicePhoto(@AuthenticationPrincipal UserContext userContext,
                                                            @PathVariable("businessId") Integer businessId,
                                                            @PathVariable("serviceId") Integer serviceId,
                                                            @RequestParam(name = "file") MultipartFile multipartFile) {
        return new ResponseWithMessage(servicesService.addFile(businessId, serviceId, multipartFile, userContext), "Success upload photo");
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/services/{serviceId}", method = RequestMethod.PUT)
    public ResponseWithMessage<ServicesDTO> editService(@AuthenticationPrincipal UserContext userContext,
                                                        @PathVariable("businessId") Integer businessId,
                                                        @PathVariable("serviceId") Integer serviceId,
                                                        @Valid @RequestBody CreateServicesDTO createServicesDTO,
                                                        BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Service fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage<>(servicesService.editServices(userContext, businessId, serviceId, createServicesDTO), "Service edited successful");
    }
}
