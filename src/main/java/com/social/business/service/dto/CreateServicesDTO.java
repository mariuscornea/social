package com.social.business.service.dto;

import com.social.core.annotation.NotEmptyList;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Data
public class CreateServicesDTO {

    @NotEmpty
    private String name;

    @NotEmpty
    private String hour;

    @NotEmptyList
    private List<String> benefitsTags= new ArrayList<>();

    @NotEmptyList
    private List<String> productTags= new ArrayList<>();

    @NotEmpty
    private String description;
}
