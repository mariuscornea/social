package com.social.business.service.dto;

import lombok.Data;

import java.util.List;

@Data
public class ServicesDTO {

    private Integer id;
    private String name;
    private String photoSrc;
    private String hour;
    private List<String> benefitsTags;
    private List<String> productTags;
    private String description;
}
