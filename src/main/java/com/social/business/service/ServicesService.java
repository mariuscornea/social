package com.social.business.service;

import com.social.business.Business;
import com.social.business.BusinessRepository;
import com.social.business.service.dto.CreateServicesDTO;
import com.social.business.service.dto.ServicesDTO;
import com.social.core.exception.EntityNotFoundException;
import com.social.file.FileService;
import com.social.security.model.UserContext;
import com.social.tags.TagUtils;
import com.social.tags.benefit.BenefitTagConverter;
import com.social.tags.benefit.BenefitTagRepository;
import com.social.tags.product.ProductTagConverter;
import com.social.tags.product.ProductTagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@Transactional
public class ServicesService {

    @Autowired
    private ServicesRepository servicesRepository;

    @Autowired
    private ServicesConverter servicesConverter;

    @Autowired
    private BusinessRepository businessRepository;

    @Autowired
    private BenefitTagRepository benefitTagRepository;

    @Autowired
    private BenefitTagConverter benefitTagConverter;

    @Autowired
    private ProductTagRepository productTagRepository;

    @Autowired
    private ProductTagConverter productTagConverter;

    @Autowired
    private TagUtils tagUtils;

    @Autowired
    private FileService fileService;

    public ServicesDTO findServicesById(Integer serviceId) {
        Services services = servicesRepository.findOne(serviceId);
        return servicesConverter.toDTO(services);
    }

    public ServicesDTO createServices(UserContext userContext, Integer businessId, CreateServicesDTO createServicesDTO) {
        Services services = servicesConverter.toEntity(createServicesDTO);

        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));

        services.setBusiness(business);

        services.setBenefitTags(tagUtils.checkBenefitTags(createServicesDTO.getBenefitsTags()));
        services.setProductTags(tagUtils.checkProductTags(createServicesDTO.getProductTags()));
        servicesRepository.save(services);
        return servicesConverter.toDTO(services);
    }

    public ServicesDTO editServices(UserContext userContext, Integer businessId, Integer serviceId, CreateServicesDTO createServicesDTO) {
        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));

        Services services = servicesRepository.findByIdAndBusinessId(serviceId, business.getId())
                .orElseThrow(() -> new EntityNotFoundException("Service not found by id:" + serviceId));

        servicesConverter.toEntity(services, createServicesDTO);

        services.setBenefitTags(tagUtils.checkBenefitTags(createServicesDTO.getBenefitsTags()));
        services.setProductTags(tagUtils.checkProductTags(createServicesDTO.getProductTags()));
        servicesRepository.save(services);
        return servicesConverter.toDTO(services);
    }

    public List<ServicesDTO> findServices(Integer businessId) {
        return servicesConverter.toDTOList(servicesRepository.findByBusinessIdOrderByIdDesc(businessId));
    }

    public ServicesDTO addFile(Integer businessId, Integer serviceId, MultipartFile multipartFile, UserContext userContext) {
        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));

        Services services = servicesRepository.findByIdAndBusinessId(serviceId, business.getId())
                .orElseThrow(() -> new EntityNotFoundException("Service not found by id:" + serviceId));
        String filePath = fileService.saveFileToDisk(userContext.getUsername(), multipartFile);
        services.setPhotoSrc(filePath);
        servicesRepository.save(services);
        return servicesConverter.toDTO(services);
    }

    public List<String> findBenefitTags() {
        return benefitTagConverter.toList(benefitTagRepository.findAll());
    }

    public List<String> findProductTags() {
        return productTagConverter.toList(productTagRepository.findAll());
    }
}
