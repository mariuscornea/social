package com.social.business.teammember;

import com.social.business.Business;
import com.social.business.BusinessRepository;
import com.social.business.teammember.dto.CreateTeamMemberDTO;
import com.social.business.teammember.dto.TeamMemberDTO;
import com.social.core.exception.EntityNotFoundException;
import com.social.file.FileService;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@Transactional
public class TeamMemberService {

    @Autowired
    private TeamMemberRepository teamMemberRepository;

    @Autowired
    private TeamMemberConverter teamMemberConverter;

    @Autowired
    private BusinessRepository businessRepository;

    @Autowired
    private FileService fileService;

    public TeamMemberDTO findTeamMemberById(Integer businessId, Integer teamMemberId) {
        TeamMember teamMember = teamMemberRepository.findByIdAndBusinessId(teamMemberId, businessId)
                .orElseThrow(() -> new EntityNotFoundException("TeamMember not found by id:" + teamMemberId));
        return teamMemberConverter.toDTO(teamMember);
    }

    public TeamMemberDTO createTeamMember(UserContext userContext, Integer businessId, CreateTeamMemberDTO createTeamMemberDTO) {
        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));

        TeamMember teamMember = teamMemberConverter.toEntity(createTeamMemberDTO);
        teamMember.setBusiness(business);

        teamMemberRepository.save(teamMember);
        return teamMemberConverter.toDTO(teamMember);
    }

    public TeamMemberDTO editTeamMember(UserContext userContext, Integer businessId, Integer teamMemberId, CreateTeamMemberDTO createTeamMemberDTO) {
        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));

        TeamMember teamMember = teamMemberRepository.findByIdAndBusinessId(teamMemberId, businessId)
                .orElseThrow(() -> new EntityNotFoundException("TeamMember not found by id:" + teamMemberId));
        teamMemberConverter.toEntity(teamMember, createTeamMemberDTO);

        teamMemberRepository.save(teamMember);
        return teamMemberConverter.toDTO(teamMember);
    }

    public List<TeamMemberDTO> findTeamMembers(Integer businessId) {
        return teamMemberConverter.toDTOList(teamMemberRepository.findByBusinessIdOrderByIdDesc(businessId));
    }

    public TeamMemberDTO addFile(Integer businessId, Integer teamMemberId, MultipartFile multipartFile, UserContext userContext) {
        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));

        TeamMember teamMember = teamMemberRepository.findByIdAndBusinessId(teamMemberId, businessId)
                .orElseThrow(() -> new EntityNotFoundException("TeamMember not found by id:" + teamMemberId));
        String filePath = fileService.saveFileToDisk(userContext.getUsername(), multipartFile);
        teamMember.setPhotoSrc(filePath);
        teamMemberRepository.save(teamMember);
        return teamMemberConverter.toDTO(teamMember);
    }
}
