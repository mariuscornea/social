package com.social.business.teammember;

import com.social.core.repo.BaseJpaRepository;

import java.util.List;
import java.util.Optional;

public interface TeamMemberRepository extends BaseJpaRepository<TeamMember, Integer>{

    Optional<TeamMember> findByIdAndBusinessId(Integer teamMemberId, Integer businessId);

    List<TeamMember> findByBusinessIdOrderByIdDesc(Integer businessId);
}
