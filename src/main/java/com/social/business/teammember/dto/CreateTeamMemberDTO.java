package com.social.business.teammember.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class CreateTeamMemberDTO {

    @NotEmpty
    private String name;

    @NotEmpty
    private String surname;

    @NotEmpty
    private String nickname;

    @NotEmpty
    private String description;
}
