package com.social.business.teammember.dto;

import lombok.Data;

@Data
public class TeamMemberDTO {

    private Integer id;
    private String name;
    private String photoSrc;
    private String surname;
    private String nickname;
    private String description;
}
