package com.social.business.teammember;

import com.social.ResponseWithMessage;
import com.social.business.teammember.dto.CreateTeamMemberDTO;
import com.social.business.teammember.dto.TeamMemberDTO;
import com.social.core.exception.FieldsException;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class TeamMemberController {

    @Autowired
    private TeamMemberService teamMemberService;

    @RequestMapping(value = "/auth/businesses/{businessId}/team-members", method = RequestMethod.GET)
    public List<TeamMemberDTO> getTeamMember(@PathVariable("businessId") Integer businessId) {
        return teamMemberService.findTeamMembers(businessId);
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/team-members/{teamMemberId}", method = RequestMethod.GET)
    public TeamMemberDTO getTeamMemberById(@PathVariable("businessId") Integer businessId,
                                           @PathVariable("teamMemberId") Integer teamMemberId) {
        return teamMemberService.findTeamMemberById(businessId, teamMemberId);
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/team-members", method = RequestMethod.POST)
    public ResponseWithMessage<TeamMemberDTO> createTeamMember(@AuthenticationPrincipal UserContext userContext,
                                                               @PathVariable("businessId") Integer businessId,
                                                               @Valid @RequestBody CreateTeamMemberDTO createTeamMemberDTO,
                                                               BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("TeamMember fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage<>(teamMemberService.createTeamMember(userContext, businessId, createTeamMemberDTO), "Success create teamMember");
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/team-members/{teamMemberId}/photos", method = RequestMethod.PUT)
    public ResponseWithMessage<TeamMemberDTO> addTeamMemberPhoto(@AuthenticationPrincipal UserContext userContext,
                                                                 @PathVariable("businessId") Integer businessId,
                                                                 @PathVariable("teamMemberId") Integer teamMemberId,
                                                                 @RequestParam(name = "file") MultipartFile multipartFile) {
        return new ResponseWithMessage(teamMemberService.addFile(businessId, teamMemberId, multipartFile, userContext), "Successful upload for photo");
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/team-members/{teamMemberId}", method = RequestMethod.PUT)
    public ResponseWithMessage<TeamMemberDTO> editService(@AuthenticationPrincipal UserContext userContext,
                                                          @PathVariable("businessId") Integer businessId,
                                                          @PathVariable("teamMemberId") Integer teamMemberId,
                                                          @Valid @RequestBody CreateTeamMemberDTO createTeamMemberDTO,
                                                          BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("TeamMember fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage<>(teamMemberService.editTeamMember(userContext, businessId, teamMemberId, createTeamMemberDTO), "Service edited successful");
    }
}
