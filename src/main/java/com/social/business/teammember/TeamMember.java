package com.social.business.teammember;

import com.social.business.Business;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class TeamMember {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Business business;

    @Column
    private String photoSrc;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String surname;

    @Column(nullable = false)
    private String nickname;

    @Column(nullable = false)
    private String description;

}
