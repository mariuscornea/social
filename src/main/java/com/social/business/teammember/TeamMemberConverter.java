package com.social.business.teammember;

import com.social.business.teammember.dto.CreateTeamMemberDTO;
import com.social.business.teammember.dto.TeamMemberDTO;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TeamMemberConverter {

    public TeamMemberDTO toDTO(TeamMember teamMember) {
        TeamMemberDTO teamMemberDTO = new TeamMemberDTO();
        teamMemberDTO.setId(teamMember.getId());
        teamMemberDTO.setName(teamMember.getName());
        teamMemberDTO.setPhotoSrc(teamMember.getPhotoSrc());
        teamMemberDTO.setName(teamMember.getName());
        teamMemberDTO.setSurname(teamMember.getSurname());
        teamMemberDTO.setNickname(teamMember.getNickname());
        teamMemberDTO.setDescription(teamMember.getDescription());

        return teamMemberDTO;
    }

    public List<TeamMemberDTO> toDTOList(List<TeamMember> teamMembers) {
        return teamMembers.stream().map(this::toDTO).collect(Collectors.toList());
    }

    public TeamMember toEntity(CreateTeamMemberDTO createTeamMemberDTO){
        TeamMember teamMember = new TeamMember();
        teamMember.setName(createTeamMemberDTO.getName());
        teamMember.setSurname(createTeamMemberDTO.getSurname());
        teamMember.setNickname(createTeamMemberDTO.getNickname());
        teamMember.setDescription(createTeamMemberDTO.getDescription());

        return teamMember;
    }

    public TeamMember toEntity(TeamMember teamMember, CreateTeamMemberDTO createTeamMemberDTO){
        teamMember.setName(createTeamMemberDTO.getName());
        teamMember.setSurname(createTeamMemberDTO.getSurname());
        teamMember.setNickname(createTeamMemberDTO.getNickname());
        teamMember.setDescription(createTeamMemberDTO.getDescription());

        return teamMember;
    }
}
