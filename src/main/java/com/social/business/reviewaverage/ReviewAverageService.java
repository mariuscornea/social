package com.social.business.reviewaverage;

import com.social.business.Business;
import com.social.business.review.Review;
import com.social.business.reviewaverage.dto.ReviewAverageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional
public class ReviewAverageService {

    @Autowired
    private ReviewAverageRepository reviewAverageRepository;
    @Autowired
    private ReviewAverageConverter reviewAverageConverter;

    public ReviewAverageDTO findByBusinessId(Integer businessId){
        return reviewAverageConverter.toDTO(reviewAverageRepository.findByBusinessId(businessId));
    }


    public void addEmptyReview(Business business){
        ReviewAverage reviewAverage = new ReviewAverage();
        reviewAverage.setBusiness(business);
        reviewAverage.setProperty1(BigDecimal.ZERO);
        reviewAverage.setProperty2(BigDecimal.ZERO);
        reviewAverage.setProperty3(BigDecimal.ZERO);
        reviewAverage.setProperty4(BigDecimal.ZERO);
        reviewAverage.setProperty5(BigDecimal.ZERO);
        reviewAverageRepository.save(reviewAverage);
    }

    public void commuteReviewAverage(List<Review> reviews, Integer businessId){
        ReviewAverage reviewAverage = reviewAverageRepository.findByBusinessId(businessId);
        commute(reviewAverage, reviews);
        reviewAverageRepository.save(reviewAverage);
    }

    private void commute(ReviewAverage reviewAverage, List<Review> reviews){
        int property1 = 0, property2 = 0, property3 = 0, property4 = 0, property5 = 0;

        for(Review review : reviews){
            property1 += review.getProperty1();
            property2 += review.getProperty2();
            property3 += review.getProperty3();
            property4 += review.getProperty4();
            property5 += review.getProperty5();
        }
        BigDecimal size = new BigDecimal(reviews.size());
        reviewAverage.setProperty1(new BigDecimal(property1).divide(size, 1, BigDecimal.ROUND_HALF_UP));
        reviewAverage.setProperty2(new BigDecimal(property2).divide(size, 1, BigDecimal.ROUND_HALF_UP));
        reviewAverage.setProperty3(new BigDecimal(property3).divide(size, 1, BigDecimal.ROUND_HALF_UP));
        reviewAverage.setProperty4(new BigDecimal(property4).divide(size, 1, BigDecimal.ROUND_HALF_UP));
        reviewAverage.setProperty5(new BigDecimal(property5).divide(size, 1, BigDecimal.ROUND_HALF_UP));
    }
}
