package com.social.business.reviewaverage.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReviewAverageDTO {

    private BigDecimal property1;
    private BigDecimal property2;
    private BigDecimal property3;
    private BigDecimal property4;
    private BigDecimal property5;
}
