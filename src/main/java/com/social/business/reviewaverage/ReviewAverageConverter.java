package com.social.business.reviewaverage;

import com.social.business.reviewaverage.dto.ReviewAverageDTO;
import org.springframework.stereotype.Component;

@Component
public class ReviewAverageConverter {

    public ReviewAverageDTO toDTO(ReviewAverage reviewAverage){
        ReviewAverageDTO reviewAverageDTO = new ReviewAverageDTO();
        reviewAverageDTO.setProperty1(reviewAverage.getProperty1());
        reviewAverageDTO.setProperty2(reviewAverage.getProperty2());
        reviewAverageDTO.setProperty3(reviewAverage.getProperty3());
        reviewAverageDTO.setProperty4(reviewAverage.getProperty4());
        reviewAverageDTO.setProperty5(reviewAverage.getProperty5());

        return reviewAverageDTO;
    }
}
