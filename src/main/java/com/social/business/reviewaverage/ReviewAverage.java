package com.social.business.reviewaverage;

import com.social.business.Business;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;

@Data
@Entity
public class ReviewAverage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Business business;

    @Column(nullable = false)
    private BigDecimal property1;

    @Column(nullable = false)
    @Digits(integer=1, fraction=1)
    private BigDecimal property2;

    @Column(nullable = false)
    @Digits(integer=1, fraction=1)
    private BigDecimal property3;

    @Column(nullable = false)
    @Digits(integer=1, fraction=1)
    private BigDecimal property4;

    @Column(nullable = false)
    @Digits(integer=1, fraction=1)
    private BigDecimal property5;
}
