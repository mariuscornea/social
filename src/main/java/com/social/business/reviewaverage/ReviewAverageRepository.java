package com.social.business.reviewaverage;

import com.social.core.repo.BaseJpaRepository;

public interface ReviewAverageRepository extends BaseJpaRepository<ReviewAverage, Integer> {

    ReviewAverage findByBusinessId(Integer businessId);
}
