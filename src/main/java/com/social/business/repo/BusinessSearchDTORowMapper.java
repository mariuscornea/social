package com.social.business.repo;

import com.social.business.dto.BusinessSearchDTO;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BusinessSearchDTORowMapper implements RowMapper<BusinessSearchDTO>{

    @Nullable
    @Override
    public BusinessSearchDTO mapRow(ResultSet resultSet, int i) throws SQLException {
        BusinessSearchDTO businessSearchDTO = new BusinessSearchDTO();
        businessSearchDTO.setId(resultSet.getInt("id"));
        businessSearchDTO.setName(resultSet.getString("name"));
        businessSearchDTO.setCategoryName(resultSet.getString("categoryName"));
        businessSearchDTO.setIsExpert(resultSet.getBoolean("isExpert"));
        businessSearchDTO.setPictureSrc(resultSet.getString("pictureSrc"));
        return businessSearchDTO;
    }
}
