package com.social.business.repo;

import com.social.business.dto.BusinessSearchDTO;
import com.social.core.repo.BaseRepository;
import com.social.core.sql.SQLQueryBuilder;
import com.social.pagination.PaginationRepo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class BusinessRepositoryImpl extends BaseRepository implements BusinessRepositoryCustom {

    private static final BusinessSearchDTORowMapper BUSINESS_SEARCH_ROW_MAPPER = new BusinessSearchDTORowMapper();

    @Autowired
    private PaginationRepo<BusinessSearchDTO> paginationRepo;

    public Page<BusinessSearchDTO> searchBusiness(Map<String, String> parameters, Pageable pageable) {
        SQLQueryBuilder queryBuilder = new SQLQueryBuilder()
                .select()
                .column("b.id", "id")
                .column("b.name", "name")
                .column("b.is_expert", "isExpert")
                .column("b.picture_src", "pictureSrc")
                .column("c.name", "categoryName")
                .from("business", "b")
                .innerJoin("category", "c")
                .append(" ON b.category_id = c.id");
        if (parameters.containsKey("category") && StringUtils.isNotBlank(parameters.get("category"))) {
            queryBuilder.append(" AND c.name = ?", parameters.get("category"));
        }
        queryBuilder.where().append("1 = 1");
        if (parameters.containsKey("name") && StringUtils.isNotBlank(parameters.get("name"))) {
            queryBuilder.append(" AND b.name ILIKE ?", "%" + parameters.get("name") + "%");
        }
        if (parameters.containsKey("longitude") &&
                parameters.containsKey("latitude") &&
                StringUtils.isNotBlank(parameters.get("longitude")) &&
                StringUtils.isNotBlank(parameters.get("latitude"))) {
            queryBuilder.append(" AND ST_DWithin(b.geometry, ST_GeographyFromText('SRID=4326; POINT(' || ? || ' ' || ? || ')'), 1000)", parameters.get("longitude"), parameters.get("latitude"));
        }
        if(parameters.containsKey("isExpert") && StringUtils.isNotBlank(parameters.get("isExpert"))){
            queryBuilder.append(" AND b.is_expert = ?", new Boolean(parameters.get("isExpert")));
        }
        return paginationRepo.getPaginatedResult(queryBuilder, BUSINESS_SEARCH_ROW_MAPPER, pageable);
    }
}
