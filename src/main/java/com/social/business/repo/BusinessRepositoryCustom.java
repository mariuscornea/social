package com.social.business.repo;

import com.social.business.dto.BusinessSearchDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface BusinessRepositoryCustom {

    Page<BusinessSearchDTO> searchBusiness(Map<String, String> parameters, Pageable pageable);
}
