package com.social.business.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class EditBusinessNameDTO {

    @NotEmpty
    private String name;
}
