package com.social.business.dto;

import lombok.Data;

@Data
public class BusinessNameDTO {

    private Integer id;
    private String name;
    private Boolean isExpert;
}
