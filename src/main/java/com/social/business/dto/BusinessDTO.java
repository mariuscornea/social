package com.social.business.dto;

import com.social.business.reviewaverage.dto.ReviewAverageDTO;
import lombok.Data;

@Data
public class BusinessDTO {

    private Integer id;
    private String categoryName;
    private Boolean isCompleted;
    private String name;
    private String pictureSrc;
    private String coverBigText;
    private String coverSmallText;
    private Boolean isExpert;
    private Boolean hasDeal;

    private ReviewAverageDTO reviewAverage;
}
