package com.social.business.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Data
public class BusinessSearchDTO {

    private Integer id;
    private String name;
    private String categoryName;
    private Long publicationMillis =  LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli();
    private String pictureSrc;
    private Boolean isExpert;

}
