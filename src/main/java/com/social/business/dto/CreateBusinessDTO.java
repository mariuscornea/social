package com.social.business.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CreateBusinessDTO {

    @NotEmpty
    private String name;

    @NotEmpty
    private String categoryName;

    @NotNull
    private Boolean isExpert;
}
