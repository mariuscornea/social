package com.social.business.about;

import com.social.business.Business;
import com.social.business.BusinessRepository;
import com.social.business.about.dto.AboutDTO;
import com.social.business.about.dto.CreateAboutDTO;
import com.social.core.exception.EntityNotFoundException;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AboutService {

    @Autowired
    private AboutRepository aboutRepository;

    @Autowired
    private AboutConverter aboutConverter;

    @Autowired
    private BusinessRepository businessRepository;

    public AboutDTO findAbout(Integer businessId) {
        About about = aboutRepository.findByBusinessId(businessId);
        return aboutConverter.toDTO(about);
    }

    public AboutDTO createOrEditAbout(UserContext userContext, Integer businessId, CreateAboutDTO createAboutDTO) {
        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));

        About about = aboutRepository.findByBusinessId(business.getId());
        about = aboutConverter.toEntity(about, createAboutDTO);
        about.setBusiness(business);
        aboutRepository.save(about);
        return aboutConverter.toDTO(about);
    }
}
