package com.social.business.about.dto;

import lombok.Data;

import javax.validation.constraints.Email;

@Data
public class CreateAboutDTO {

    private String country;
    private String city;
    private String address;
    @Email
    private String email;
    private String telephone1;
    private String telephone2;
    private String website;
    private String other;
}
