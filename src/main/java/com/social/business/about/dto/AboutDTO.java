package com.social.business.about.dto;

import lombok.Data;

@Data
public class AboutDTO {

    private Integer id;
    private String country;
    private String city;
    private String address;
    private String email;
    private String telephone1;
    private String telephone2;
    private String website;
    private String other;
}
