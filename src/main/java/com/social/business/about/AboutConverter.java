package com.social.business.about;

import com.social.business.about.dto.AboutDTO;
import com.social.business.about.dto.CreateAboutDTO;
import org.springframework.stereotype.Component;

@Component
public class AboutConverter {

    public About toEntity(CreateAboutDTO createAboutDTO) {
        About about = new About();
        return toEntity(about, createAboutDTO);
    }

    public About toEntity(About about, CreateAboutDTO createAboutDTO) {
        if(about == null){
            about = new About();
        }
        about.setCountry(createAboutDTO.getCountry());
        about.setCity(createAboutDTO.getCity());
        about.setAddress(createAboutDTO.getAddress());
        about.setEmail(createAboutDTO.getEmail());
        about.setTelephone1(createAboutDTO.getTelephone1());
        about.setTelephone2(createAboutDTO.getTelephone2());
        about.setWebsite(createAboutDTO.getWebsite());
        about.setOther(createAboutDTO.getOther());
        return about;
    }

    public AboutDTO toDTO(About about) {
        AboutDTO aboutDTO = new AboutDTO();
        if (about == null) {
            return aboutDTO;
        }
        aboutDTO.setId(about.getId());
        aboutDTO.setCountry(about.getCountry());
        aboutDTO.setCity(about.getCity());
        aboutDTO.setAddress(about.getAddress());
        aboutDTO.setEmail(about.getEmail());
        aboutDTO.setTelephone1(about.getTelephone1());
        aboutDTO.setTelephone2(about.getTelephone2());
        aboutDTO.setWebsite(about.getWebsite());
        aboutDTO.setOther(about.getOther());
        return aboutDTO;
    }
}
