package com.social.business.about;

import com.social.core.repo.BaseJpaRepository;

public interface AboutRepository extends BaseJpaRepository<About, Integer> {

    About findByBusinessId(Integer businessId);
}
