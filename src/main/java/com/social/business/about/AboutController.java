package com.social.business.about;

import com.social.ResponseWithMessage;
import com.social.business.about.dto.AboutDTO;
import com.social.business.about.dto.CreateAboutDTO;
import com.social.core.exception.FieldsException;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api")
public class AboutController {

    @Autowired
    private AboutService aboutService;

    @RequestMapping(value = "/businesses/{businessId}/about", method = RequestMethod.GET)
    public AboutDTO getAbout(@PathVariable("businessId") Integer businessId){
        return aboutService.findAbout(businessId);
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/about", method = RequestMethod.POST)
    public ResponseWithMessage<AboutDTO> createAbout(@AuthenticationPrincipal UserContext userContext,
                                                     @PathVariable("businessId") Integer businessId,
                                                     @Valid @RequestBody CreateAboutDTO createAboutDTO,
                                                     BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("About fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage(aboutService.createOrEditAbout(userContext, businessId, createAboutDTO), "AboutUs saved with success");
    }
}
