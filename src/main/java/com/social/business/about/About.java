package com.social.business.about;

import com.social.business.Business;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class About {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Business business;

    @Column
    private String country;

    @Column
    private String city;

    @Column
    private String address;

    @Column
    private String email;

    @Column
    private String telephone1;

    @Column
    private String telephone2;

    @Column
    private String website;

    @Column
    private String other;
}
