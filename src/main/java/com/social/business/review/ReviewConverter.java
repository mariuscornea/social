package com.social.business.review;

import com.social.business.review.dto.CreateReviewDTO;
import com.social.business.review.dto.ReviewDTO;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
public class ReviewConverter {

    public Review toEntity(CreateReviewDTO createReviewDTO){
        Review review = new Review();
        review.setProperty1(createReviewDTO.getProperty1());
        review.setProperty2(createReviewDTO.getProperty2());
        review.setProperty3(createReviewDTO.getProperty3());
        review.setProperty4(createReviewDTO.getProperty4());
        review.setProperty5(createReviewDTO.getProperty5());
        return review;
    }

    public ReviewDTO toDTO(Review review){
        ReviewDTO reviewDTO = new ReviewDTO();
        reviewDTO.setId(review.getId());
        reviewDTO.setUsername(review.getAccount().getFirstName() + " " + review.getAccount().getLastName());
        reviewDTO.setProperty1(review.getProperty1());
        reviewDTO.setProperty2(review.getProperty2());
        reviewDTO.setProperty3(review.getProperty3());
        reviewDTO.setProperty4(review.getProperty4());
        reviewDTO.setProperty5(review.getProperty5());

        return reviewDTO;
    }

    public Page<ReviewDTO> toDTOPage(Page<Review> reviews){
        return reviews.map(this::toDTO);
    }
}
