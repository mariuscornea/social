package com.social.business.review;

import com.social.account.Account;
import com.social.business.Business;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
public class Review {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Account account;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Business business;

    @Column(nullable = false)
    private Integer property1;

    @Column(nullable = false)
    private Integer property2;

    @Column(nullable = false)
    private Integer property3;

    @Column(nullable = false)
    private Integer property4;

    @Column(nullable = false)
    private Integer property5;

    @Column
    private LocalDateTime creationDate = LocalDateTime.now();
}
