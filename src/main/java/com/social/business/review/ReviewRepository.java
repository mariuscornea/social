package com.social.business.review;

import com.social.core.repo.BaseJpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ReviewRepository extends BaseJpaRepository<Review, Integer> {

    Review findByAccountIdAndBusinessId(Integer accountId, Integer businessId);

    //TODO make this query eager
    Page<Review> findByBusinessIdOrderByCreationDateDesc(Integer businessId, Pageable pageable);

    List<Review> findByBusinessId(Integer businessId);
}
