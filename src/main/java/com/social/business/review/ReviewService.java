package com.social.business.review;

import com.social.account.AccountRepository;
import com.social.business.Business;
import com.social.business.BusinessRepository;
import com.social.business.review.dto.CreateReviewDTO;
import com.social.business.review.dto.ReviewDTO;
import com.social.business.reviewaverage.ReviewAverageService;
import com.social.core.exception.ConflictException;
import com.social.core.exception.EntityNotFoundException;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private ReviewConverter reviewConverter;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private BusinessRepository businessRepository;

    @Autowired
    private ReviewAverageService reviewAverageService;

    public void createReview(UserContext userContext, Integer businessId, CreateReviewDTO createReviewDTO) {
        Review review = reviewRepository.findByAccountIdAndBusinessId(userContext.getId(), businessId);
        if (review != null) {
            throw new ConflictException("You already reviewed the business");
        }
        review = reviewConverter.toEntity(createReviewDTO);
        review.setAccount(accountRepository.getOne(userContext.getId()));
        Business business = businessRepository.findById(businessId)
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));
        review.setBusiness(business);
        reviewRepository.save(review);
        List<Review> reviews = reviewRepository.findByBusinessId(businessId);
        reviewAverageService.commuteReviewAverage(reviews, businessId);
    }

    public Page<ReviewDTO> listReviews(Integer businessId, Pageable pageable) {
        return reviewConverter.toDTOPage(reviewRepository.findByBusinessIdOrderByCreationDateDesc(businessId, pageable));
    }
}
