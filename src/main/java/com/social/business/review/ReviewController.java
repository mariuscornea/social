package com.social.business.review;

import com.social.ResponseWithMessage;
import com.social.business.dto.BusinessDTO;
import com.social.business.review.dto.CreateReviewDTO;
import com.social.business.review.dto.ReviewDTO;
import com.social.core.exception.FieldsException;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @RequestMapping(value = "/auth/businesses/{businessId}/reviews", method = RequestMethod.POST)
    public ResponseWithMessage<BusinessDTO> createReview(@AuthenticationPrincipal UserContext userContext,
                                                         @PathVariable("businessId") Integer businessId,
                                                         @Valid @RequestBody CreateReviewDTO createReviewDTO,
                                                         BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Review fields are incorrect", "msg.key", result);
        }
        reviewService.createReview(userContext, businessId, createReviewDTO);
        return new ResponseWithMessage("Review with success");
    }

    @RequestMapping(value = "/businesses/{businessId}/reviews", method = RequestMethod.GET)
    public Page<ReviewDTO> getReviewsForBusiness(@PathVariable("businessId") Integer businessId,
                                                 @PageableDefault(page = 0, size = 20) Pageable pageable) {
        return reviewService.listReviews(businessId, pageable);
    }
}
