package com.social.business.review.dto;

import lombok.Data;

@Data
public class ReviewDTO {

    private Integer id;
    private String username;
    private Integer property1;
    private Integer property2;
    private Integer property3;
    private Integer property4;
    private Integer property5;
}
