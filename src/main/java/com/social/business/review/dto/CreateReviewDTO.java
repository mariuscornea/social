package com.social.business.review.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
public class CreateReviewDTO {

    @Min(1)
    @Max(5)
    private Integer property1;

    @Min(1)
    @Max(5)
    private Integer property2;

    @Min(1)
    @Max(5)
    private Integer property3;

    @Min(1)
    @Max(5)
    private Integer property4;

    @Min(1)
    @Max(5)
    private Integer property5;
}
