package com.social.business.product;

import com.social.business.product.dto.CreateProductDTO;
import com.social.business.product.dto.ProductDTO;
import com.social.tags.benefit.BenefitTagConverter;
import com.social.tags.product.ProductTagConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductConverter {

    @Autowired
    private BenefitTagConverter benefitTagConverter;

    @Autowired
    private ProductTagConverter productTagConverter;

    public ProductDTO toDTO(Product product) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(product.getId());
        productDTO.setName(product.getName());
        productDTO.setPhotoSrc(product.getPhotoSrc());
        productDTO.setStock(product.getStock());
        productDTO.setDescription(product.getDescription());
        productDTO.setBenefitsTags(benefitTagConverter.toList(product.getBenefitTags()));
        productDTO.setProductTags(productTagConverter.toList(product.getProductTags()));

        return productDTO;
    }

    public Product toEntity(CreateProductDTO createProductDTO) {
        Product product = new Product();
        product.setName(createProductDTO.getName());
        product.setStock(createProductDTO.getStock());
        product.setDescription(createProductDTO.getDescription());

        return product;
    }

    public Product toEntity(Product product, CreateProductDTO createProductDTO) {
        product.setName(createProductDTO.getName());
        product.setStock(createProductDTO.getStock());
        product.setDescription(createProductDTO.getDescription());

        return product;
    }

    public List<ProductDTO> toDTOList(List<Product> products) {
        return products.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
