package com.social.business.product;

import com.social.business.Business;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Business business;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Integer stock;

    @Column
    private String photoSrc;

    @Column
    private String benefitTags;

    @Column
    private String productTags;

    @Column(nullable = false)
    private String description;
}
