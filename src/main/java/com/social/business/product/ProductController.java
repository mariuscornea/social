package com.social.business.product;

import com.social.ResponseWithMessage;
import com.social.business.product.dto.CreateProductDTO;
import com.social.business.product.dto.ProductDTO;
import com.social.core.exception.FieldsException;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/auth/businesses/{businessId}/products", method = RequestMethod.GET)
    public List<ProductDTO> getProducts(@AuthenticationPrincipal UserContext userContext,
                                        @PathVariable("businessId") Integer businessId) {
        return productService.findProducts(businessId);
    }

    @RequestMapping(value = "/businesses/{businessId}/products", method = RequestMethod.GET)
    public List<ProductDTO> getProducts(@PathVariable("businessId") Integer businessId) {
        return productService.findProducts(businessId);
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/products/{productId}", method = RequestMethod.GET)
    public ProductDTO getProductById(@PathVariable("businessId") Integer businessId,
                                     @PathVariable("productId") Integer productId) {
        return productService.findProductById(businessId, productId);
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/products", method = RequestMethod.POST)
    public ResponseWithMessage<ProductDTO> createProduct(@AuthenticationPrincipal UserContext userContext,
                                                         @PathVariable("businessId") Integer businessId,
                                                         @Valid @RequestBody CreateProductDTO createProductDTO,
                                                         BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Product fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage<>(productService.createProduct(userContext, businessId, createProductDTO), "Success create product");
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/products/{productId}/photos", method = RequestMethod.PUT)
    public ResponseWithMessage<ProductDTO> addProductPhoto(@AuthenticationPrincipal UserContext userContext,
                                                           @PathVariable("businessId") Integer businessId,
                                                           @PathVariable("productId") Integer productId,
                                                           @RequestParam(name = "file") MultipartFile multipartFile) {
        return new ResponseWithMessage(productService.addFile(businessId, productId, multipartFile, userContext), "Successful upload for photo");
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/products/{productId}", method = RequestMethod.PUT)
    public ResponseWithMessage<ProductDTO> editService(@AuthenticationPrincipal UserContext userContext,
                                                       @PathVariable("businessId") Integer businessId,
                                                       @PathVariable("productId") Integer productId,
                                                       @Valid @RequestBody CreateProductDTO createProductDTO,
                                                       BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Product fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage<>(productService.editProduct(userContext, businessId, productId, createProductDTO), "Service edited successful");
    }
}
