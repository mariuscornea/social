package com.social.business.product;

import com.social.business.Business;
import com.social.business.BusinessRepository;
import com.social.business.product.dto.CreateProductDTO;
import com.social.business.product.dto.ProductDTO;
import com.social.core.exception.EntityNotFoundException;
import com.social.file.FileService;
import com.social.security.model.UserContext;
import com.social.tags.TagUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@Transactional
public class ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private ProductConverter productConverter;
    @Autowired
    private BusinessRepository businessRepository;
    @Autowired
    private FileService fileService;
    @Autowired
    private TagUtils tagUtils;

    public ProductDTO findProductById(Integer businessId, Integer productId) {
        Product product = productRepository.findByIdAndBusinessId(productId, businessId)
                .orElseThrow(() -> new EntityNotFoundException("Product not found by id:" + productId));
        return productConverter.toDTO(product);
    }

    public ProductDTO createProduct(UserContext userContext, Integer businessId, CreateProductDTO createProductDTO) {
        Product product = productConverter.toEntity(createProductDTO);

        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));

        product.setBusiness(business);
        product.setBenefitTags(tagUtils.checkBenefitTags(createProductDTO.getBenefitsTags()));
        product.setProductTags(tagUtils.checkProductTags(createProductDTO.getProductTags()));
        productRepository.save(product);
        return productConverter.toDTO(product);
    }

    public ProductDTO editProduct(UserContext userContext, Integer businessId, Integer productId, CreateProductDTO createProductDTO) {
        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));
        Product product = productRepository.findByIdAndBusinessId(productId, business.getId())
                .orElseThrow(() -> new EntityNotFoundException("Product not found by id:" + productId));

        productConverter.toEntity(product, createProductDTO);
        product.setBenefitTags(tagUtils.checkBenefitTags(createProductDTO.getBenefitsTags()));
        product.setProductTags(tagUtils.checkProductTags(createProductDTO.getProductTags()));
        productRepository.save(product);
        return productConverter.toDTO(product);
    }

    public List<ProductDTO> findProducts(Integer businessId) {
        return productConverter.toDTOList(productRepository.findByBusinessIdOrderByIdDesc(businessId));
    }

    public ProductDTO addFile(Integer businessId, Integer productId, MultipartFile multipartFile, UserContext userContext) {
        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));

        Product services = productRepository.findByIdAndBusinessId(productId, business.getId())
                .orElseThrow(() -> new EntityNotFoundException("Product not found by id:" + productId));
        String filePath = fileService.saveFileToDisk(userContext.getUsername(), multipartFile);
        services.setPhotoSrc(filePath);
        productRepository.save(services);

        return productConverter.toDTO(services);
    }
}
