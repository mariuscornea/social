package com.social.business.product;

import com.social.core.repo.BaseJpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends BaseJpaRepository<Product, Integer> {

    Optional<Product> findByIdAndBusinessId(Integer productId, Integer businessId);

    List<Product> findByBusinessIdOrderByIdDesc(Integer accountId);
}
