package com.social.business.product.dto;

import lombok.Data;

import java.util.List;

@Data
public class ProductDTO {

    private Integer id;
    private String name;
    private String photoSrc;
    private Integer stock;
    private List<String> benefitsTags;
    private List<String> productTags;
    private String description;
}
