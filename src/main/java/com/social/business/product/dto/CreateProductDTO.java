package com.social.business.product.dto;

import com.social.core.annotation.NotEmptyList;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.util.ArrayList;
import java.util.List;

@Data
public class CreateProductDTO {

    @NotEmpty
    private String name;

    @Positive
    private Integer stock;

    @NotEmptyList
    private List<String> benefitsTags= new ArrayList<>();

    @NotEmptyList
    private List<String> productTags= new ArrayList<>();

    @NotEmpty
    private String description;
}
