package com.social.business;

import com.social.business.deal.Deal;
import com.social.business.dto.BusinessDTO;
import com.social.business.dto.BusinessNameDTO;
import com.social.business.dto.CreateBusinessDTO;
import com.social.business.projection.BusinessNameProj;
import com.social.security.model.UserContext;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class BusinessConverter {

    public BusinessDTO toDTO(Business business, UserContext userContext) {
        BusinessDTO businessDTO = toDTO(business);
        Optional<Deal> first;
        if (userContext != null && business.getAccount().getId().equals(userContext.getId())) {
            businessDTO.setHasDeal(null);
        } else {
            if (userContext != null) {
                if (business.getDeals() != null) {
                    first = business.getDeals().stream().filter(deal -> deal.getAccount().getId().equals(userContext.getId())).findFirst();
                } else {
                    first = Optional.empty();
                }
            } else {
                first = Optional.empty();
            }
            businessDTO.setHasDeal(first.isPresent());
        }
        return businessDTO;
    }

    public BusinessDTO toDTO(Business business) {
        BusinessDTO businessDTO = new BusinessDTO();
        businessDTO.setId(business.getId());
        businessDTO.setIsCompleted(business.getIsCompleted());
        businessDTO.setName(business.getName());
        businessDTO.setPictureSrc(business.getPictureSrc());
        businessDTO.setCoverBigText(business.getCoverBigText());
        businessDTO.setCoverSmallText(business.getCoverSmallText());
        businessDTO.setIsExpert(business.getIsExpert());
        return businessDTO;
    }

    public List<BusinessNameDTO> toBusinessNameDTO(List<BusinessNameProj> businessNameProjs) {
        return businessNameProjs.stream().map(businessNameProj -> {
            BusinessNameDTO businessNameDTO = new BusinessNameDTO();
            businessNameDTO.setId(businessNameProj.getId());
            businessNameDTO.setName(businessNameProj.getName());
            businessNameDTO.setIsExpert(businessNameDTO.getIsExpert());

            return businessNameDTO;
        }).collect(Collectors.toList());
    }

    public Business toEntity(CreateBusinessDTO createBusinessDTO) {
        Business business = new Business();
        business.setName(createBusinessDTO.getName());
        business.setIsExpert(createBusinessDTO.getIsExpert());
        return business;
    }

    public List<BusinessDTO> toDTOList(List<Business> businesses) {
        return businesses.stream()
                .map(business -> toDTO(business, null))
                .collect(Collectors.toList());
    }

}
