package com.social.business.deal;

import com.social.core.repo.BaseJpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DealRepository extends BaseJpaRepository<Deal, Integer>{

    Deal findByAccountIdAndBusinessId(Integer accountId, Integer businessId);

    //TODO do this query eager
    Page<Deal> findByAccountId(Integer accountId, Pageable pageable);
}
