package com.social.business.deal;

import com.social.business.deal.dto.DealDTO;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
public class DealConverter {

    public DealDTO toDTO(Deal deal) {
        DealDTO dealDTO = new DealDTO();
        dealDTO.setId(deal.getId());
        dealDTO.setBusinessName(deal.getBusiness().getName());
        dealDTO.setBusinessPhoto(deal.getBusiness().getPictureSrc());
        return dealDTO;
    }

    public Page<DealDTO> toDTOPage(Page<Deal> deals) {
        return deals.map(this::toDTO);
    }
}
