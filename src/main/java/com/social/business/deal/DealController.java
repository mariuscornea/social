package com.social.business.deal;

import com.social.ResponseWithMessage;
import com.social.business.deal.dto.DealDTO;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class DealController {

    @Autowired
    private DealService dealService;

    @RequestMapping(value = "/auth/businesses/{businessId}/deals", method = RequestMethod.POST)
    public ResponseWithMessage<Void> createDeal(@AuthenticationPrincipal UserContext userContext,
                                                @PathVariable("businessId") Integer businessId) {
        dealService.createDeal(userContext, businessId);
        return new ResponseWithMessage("Deal made with success");
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/deals", method = RequestMethod.DELETE)
    public ResponseWithMessage<Void> deleteDeal(@AuthenticationPrincipal UserContext userContext,
                                                @PathVariable("businessId") Integer businessId) {
        dealService.deleteDeal(userContext, businessId);
        return new ResponseWithMessage("Deal made with success");
    }

    @RequestMapping(value = "/auth/businesses/deals", method = RequestMethod.GET)
    public Page<DealDTO> getDeals(@AuthenticationPrincipal UserContext userContext,
                                  @PageableDefault(page = 0, size = 20) Pageable pageable) {
        return dealService.listDeals(userContext, pageable);
    }
}
