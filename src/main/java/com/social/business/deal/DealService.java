package com.social.business.deal;

import com.social.account.AccountRepository;
import com.social.business.BusinessRepository;
import com.social.business.deal.dto.DealDTO;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DealService {

    @Autowired
    private DealRepository dealRepository;

    @Autowired
    private DealConverter dealConverter;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private BusinessRepository businessRepository;

    public void createDeal(UserContext userContext, Integer businessId) {
        //TODO don't allow deal with your buss
        Deal deal = dealRepository.findByAccountIdAndBusinessId(userContext.getId(), businessId);
        if (deal == null) {
            deal = new Deal();
            deal.setAccount(accountRepository.getOne(userContext.getId()));
            deal.setBusiness(businessRepository.findOne(businessId));

            dealRepository.save(deal);
        }
    }

    public void deleteDeal(UserContext userContext, Integer businessId) {
        Deal deal = dealRepository.findByAccountIdAndBusinessId(userContext.getId(), businessId);
        if (deal != null) {
            dealRepository.delete(deal);
        }
    }

    public Page<DealDTO> listDeals(UserContext userContext, Pageable pageable) {
        return dealConverter.toDTOPage(dealRepository.findByAccountId(userContext.getId(), pageable));
    }

}
