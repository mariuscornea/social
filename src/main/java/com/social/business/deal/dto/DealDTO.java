package com.social.business.deal.dto;

import lombok.Data;

@Data
public class DealDTO {

    private Integer id;
    private String businessName;
    private String businessPhoto;
}
