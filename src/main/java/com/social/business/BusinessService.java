package com.social.business;

import com.social.account.AccountRepository;
import com.social.business.dto.*;
import com.social.business.reviewaverage.ReviewAverageService;
import com.social.category.Category;
import com.social.category.CategoryRepository;
import com.social.core.exception.EntityNotFoundException;
import com.social.file.FileService;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class BusinessService {

    @Autowired
    private BusinessRepository businessRepository;

    @Autowired
    private BusinessConverter businessConverter;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private FileService fileService;

    @Autowired
    private ReviewAverageService reviewAverageService;

    public Page<BusinessSearchDTO> search(Map<String, String> parameters, Pageable pageable) {
        return businessRepository.searchBusiness(parameters, pageable);
    }

    public BusinessDTO createBusiness(UserContext userContext, CreateBusinessDTO createBusinessDTO) {
        Category category = categoryRepository.findByName(createBusinessDTO.getCategoryName())
                .orElseThrow(() -> new EntityNotFoundException("Category not found by name:" + createBusinessDTO.getCategoryName()));
        Business business = businessConverter.toEntity(createBusinessDTO);
        business.setAccount(accountRepository.getOne(userContext.getId()));
        business.setCategory(category);
        businessRepository.save(business);

        reviewAverageService.addEmptyReview(business);

        return businessConverter.toDTO(business, userContext);
    }

    public BusinessDTO editBusinessName(UserContext userContext, Integer businessId, EditBusinessNameDTO editBusinessNameDTO) {
        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));
        business.setName(editBusinessNameDTO.getName());
        businessRepository.save(business);

        return businessConverter.toDTO(business, userContext);
    }

    public BusinessDTO addFile(UserContext userContext, Integer businessId, MultipartFile multipartFile) {
        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));

        String filePath = fileService.saveFileToDisk(userContext.getUsername(), multipartFile);
        business.setPictureSrc(filePath);

        businessRepository.save(business);
        return businessConverter.toDTO(business, userContext);
    }

    public List<BusinessNameDTO> getBusinessesName(UserContext userContext) {
        return businessConverter.toBusinessNameDTO(businessRepository.findProjByAccountIdOrderByIdDesc(userContext.getId()));
    }

    public BusinessDTO getBusinessById(UserContext userContext, Integer businessId) {
        Business business = businessRepository.findOne(businessId);
        BusinessDTO businessDTO = businessConverter.toDTO(business, userContext);
        businessDTO.setReviewAverage(reviewAverageService.findByBusinessId(businessId));
        return businessDTO;
    }

    public BusinessDTO getBusinessById(Integer businessId) {
        Business business = businessRepository.findOne(businessId);
        BusinessDTO businessDTO = businessConverter.toDTO(business);
        businessDTO.setReviewAverage(reviewAverageService.findByBusinessId(businessId));
        return businessDTO;
    }

    public Page<BusinessSearchDTO> searchPage(Map<String, String> parameters, Pageable pageable) {
        Page<Business> results;
        if (parameters.containsKey("name") && parameters.containsKey("category")) {
            results = businessRepository.findByNameContainingAndCategoryName(parameters.get("name"), parameters.get("category"), pageable);
        } else if (parameters.containsKey("name")) {
            results = businessRepository.findByNameContaining(parameters.get("name"), pageable);
        } else if (parameters.containsKey("category")) {
            results = businessRepository.findByCategoryName(parameters.get("category"), pageable);
        } else {
            results = businessRepository.findAll(pageable);
        }

        Page<BusinessSearchDTO> convert = results.map(this::convertToBusinessSearchDTO);

        return convert;
    }

    private BusinessSearchDTO convertToBusinessSearchDTO(Business business) {
        final BusinessSearchDTO businessSearchDTO = new BusinessSearchDTO();
        businessSearchDTO.setId(business.getId());
        businessSearchDTO.setName(business.getName());
        businessSearchDTO.setCategoryName(business.getCategory().getName());
        businessSearchDTO.setPictureSrc(business.getPictureSrc());
        businessSearchDTO.setIsExpert(business.getIsExpert());
        return businessSearchDTO;
    }
}
