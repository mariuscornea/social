package com.social.business.projection;

public interface BusinessNameProj {

    Integer getId();
    String getName();
}
