package com.social.business;

import com.social.business.projection.BusinessNameProj;
import com.social.business.repo.BusinessRepositoryCustom;
import com.social.core.repo.BaseJpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface BusinessRepository extends BaseJpaRepository<Business, Integer>, BusinessRepositoryCustom {

    Optional<Business> findByIdAndAccountId(Integer businessId, Integer accountId);

    List<BusinessNameProj> findProjByAccountIdOrderByIdDesc(Integer accountId);

    List<Business> findByAccountIdOrderByIdDesc(Integer accountId);

    Page<Business> findByNameContaining(String name, Pageable pageable);

    Page<Business> findByCategoryName(String categoryName, Pageable pageable);

    Page<Business> findByNameContainingAndCategoryName(String name, String categoryName, Pageable pageable);

    List<Business> findTop10ByOrderByPublicationDateDesc();

//    Page<Business> findAll(Pageable pageable);
}
