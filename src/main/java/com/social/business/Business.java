package com.social.business;

import com.social.account.Account;
import com.social.business.deal.Deal;
import com.social.category.Category;
import com.vividsolutions.jts.geom.Geometry;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
public class Business {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Account account;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Category category;

    @Column(nullable = false)
    private Boolean isCompleted = false;

    @Column
    private String name;

    @Column
    private String pictureSrc;

    @Column
    private String coverBigText = "Big Text Sample For Achim";

    @Column
    private String coverSmallText = "Small Text Sample";

    @Column (columnDefinition = "geometry(Point,4326)")
    private Geometry geometry;

    @Column
    private Double latitude;

    @Column
    private Double longitude;

    @Column
    private Boolean isExpert;

    @OneToMany(mappedBy = "business", fetch = FetchType.EAGER)
    private List<Deal> deals;

    @Column
    private LocalDateTime publicationDate = LocalDateTime.now();
}
