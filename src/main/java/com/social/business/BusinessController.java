package com.social.business;

import com.social.ResponseWithMessage;
import com.social.business.dto.*;
import com.social.business.product.dto.ProductDTO;
import com.social.core.exception.FieldsException;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api")
public class BusinessController {

    @Autowired
    private BusinessService businessService;

    @RequestMapping(value = "/auth/businesses", method = RequestMethod.POST)
    public ResponseWithMessage<BusinessDTO> addBusiness(@AuthenticationPrincipal UserContext userContext,
                                                        @Valid @RequestBody CreateBusinessDTO createBusinessDTO,
                                                        BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Business fields are incorrect", "msg.key", result);
        }

        return new ResponseWithMessage(businessService.createBusiness(userContext, createBusinessDTO), "Business added with success");
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/name", method = RequestMethod.PUT)
    public ResponseWithMessage<BusinessDTO> editBusinessName(@AuthenticationPrincipal UserContext userContext,
                                                             @PathVariable("businessId") Integer businessId,
                                                             @Valid @RequestBody EditBusinessNameDTO editBusinessNameDTO,
                                                             BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Business fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage(businessService.editBusinessName(userContext, businessId, editBusinessNameDTO), "Business name edited with success");
    }

    @RequestMapping(value = "/auth/businesses/{businessId}/photos", method = RequestMethod.PUT)
    public ResponseWithMessage<ProductDTO> addBusinessPhoto(@AuthenticationPrincipal UserContext userContext,
                                                            @PathVariable("businessId") Integer businessId,
                                                            @RequestParam(name = "file") MultipartFile multipartFile) {
        return new ResponseWithMessage(businessService.addFile(userContext, businessId, multipartFile), "Successful upload for photo");
    }

    @RequestMapping(value = "/auth/businesses/name", method = RequestMethod.GET)
    public List<BusinessNameDTO> getBusinessesName(@AuthenticationPrincipal UserContext userContext) {
        return businessService.getBusinessesName(userContext);
    }

    @RequestMapping(value = "/auth/businesses/{businessId}", method = RequestMethod.GET)
    public BusinessDTO getBusinessesById(@AuthenticationPrincipal UserContext userContext,
                                         @PathVariable("businessId") Integer businessId) {
        return businessService.getBusinessById(userContext, businessId);
    }

    @RequestMapping(value = "/businesses/{businessId}", method = RequestMethod.GET)
    public BusinessDTO getBusinessesById(@PathVariable("businessId") Integer businessId) {
        return businessService.getBusinessById(businessId);
    }

    @RequestMapping(value = "/businesses/search", method = RequestMethod.GET)
    public Page<BusinessSearchDTO> search(@RequestParam Map<String, String> parameters,
                                          @PageableDefault(page = 0, size = 20) Pageable pageable) {
        parameters.values().removeIf(x -> x.equals("null"));
        return businessService.search(parameters, pageable);
    }
}
