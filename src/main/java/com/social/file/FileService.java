package com.social.file;

import com.social.core.exception.ServerOperationException;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Coordinate;
import net.coobird.thumbnailator.name.Rename;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Service
public class FileService {

    @Value("${root.path}")
    private String rootPath;

    //TODO implement purge mechanism
    public String saveFileToDisk(String username, MultipartFile file) {
        int startExtensionIndex = file.getOriginalFilename().lastIndexOf('.');
        String newFileName = file.getOriginalFilename().substring(0, startExtensionIndex) +
                LocalDateTime.now().toInstant(ZoneOffset.UTC).toEpochMilli() +
                file.getOriginalFilename().substring(startExtensionIndex, file.getOriginalFilename().length());
        final Path filePath = Paths.get(rootPath + File.separator + username + File.separator + newFileName);
        final Path directoryPath = Paths.get(rootPath + File.separator + username);
        try {
            Files.createDirectories(directoryPath);
            Files.write(filePath, file.getBytes());
//            cropFile(directoryPath.toString(), newFileName);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ServerOperationException("Something went wrong when saving the file");
        }
        return Paths.get(rootPath).getParent().relativize(filePath).toString().replace("\\", "/");
    }

    public void cropFile(String path, String filename) throws IOException {
        Path directoryPath = Paths.get(path + File.separator + "Thumbnails");
        directoryPath = Files.createDirectories(directoryPath);
        Thumbnails.of(path + File.separator + filename)
                .sourceRegion(new Coordinate(1000, 1000), 1500, 1500)
                .size(100, 100)
                .toFiles(directoryPath.toFile(), Rename.NO_CHANGE);
    }

    private void clean(Path path){
        try {
            Files.delete(path);
        } catch (IOException e) {
            e.printStackTrace();
            throw new ServerOperationException("Something went wrong when deleting the file" + path);
        }
    }
}
