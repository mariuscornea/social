package com.social.pagination;

import com.social.core.repo.BaseRepository;
import com.social.core.sql.SQLQuery;
import com.social.core.sql.SQLQueryBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaginationRepo<T> extends BaseRepository {

    public Page<T> getPaginatedResult(SQLQueryBuilder queryBuilder, RowMapper<T> rowMapper, Pageable pageable) {
        SQLQuery countQuery = queryBuilder.transformInCountQuery();

        queryBuilder.append(" OFFSET ? LIMIT ? ", pageable.getPageNumber() * pageable.getPageSize(), pageable.getPageSize());
        SQLQuery query = queryBuilder.build();
        List<T> result = jdbcTemplate.query(query.getQuery(), query.getParams(), rowMapper);

        long count = 0;
        List<Long> countResult = jdbcTemplate.queryForList(countQuery.getQuery(), countQuery.getParams(), Long.class);
        if (countResult.size() > 0) {
            count = countResult.get(0);
        }
        return new PageImpl<>(result, pageable, count);
    }

    public Page<T> getPaginatedResult(SQLQueryBuilder queryBuilder, ResultSetExtractor<List<T>> resultSetExtractor, Pageable pageable) {
        queryBuilder.append(" OFFSET ? LIMIT ? ", pageable.getPageNumber() * pageable.getPageSize(), pageable.getPageSize());
        SQLQuery query = queryBuilder.build();
        List<T> result = jdbcTemplate.query(query.getQuery(), query.getParams(), resultSetExtractor);

        SQLQuery sqlQuery = queryBuilder.transformInDistinctCountQuery("p.id").build();

        long count = 0;
        List<Long> countResult = jdbcTemplate.queryForList(sqlQuery.getQuery(), sqlQuery.getParams(), Long.class);
        if (countResult.size() > 0) {
            count = countResult.get(0);
        }
        return new PageImpl<>(result, pageable, count);
    }
}
