package com.social;


import org.hibernate.spatial.dialect.postgis.PostgisPG94Dialect;

import java.sql.Types;

public class SocialPostgisDialect extends PostgisPG94Dialect {

    public SocialPostgisDialect() {
        super();
        registerHibernateType(Types.OTHER, "com.vividsolutions.jts.geom.Geometry");
    }
}
