package com.social.registration;

import com.social.ResponseWithMessage;
import com.social.core.exception.FieldsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api")
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;


    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ResponseWithMessage<LoginDTO> registerUser(@RequestParam(name = "cropFile", defaultValue = "false") Boolean test,
                                                  @Valid @RequestBody RegistrationDTO registrationDTO,
                                                  BindingResult result) {
        if (result.hasErrors()) {
            System.out.println(result);
            throw new FieldsException("Account fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage(registrationService.register(test, registrationDTO),"Success message");
    }

    @RequestMapping(value = "/registration/{token}", method = RequestMethod.GET)
    public ResponseEntity<LoginDTO> confirmRegistration(@PathVariable("token") String token) {
        return new ResponseEntity<>(registrationService.confirmRegister(token), HttpStatus.OK);
    }

}
