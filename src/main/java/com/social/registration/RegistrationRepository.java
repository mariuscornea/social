package com.social.registration;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RegistrationRepository  extends JpaRepository<Registration, Integer>{

    Registration findByToken(String token);
}
