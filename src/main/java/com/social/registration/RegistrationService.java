package com.social.registration;

import com.social.account.Account;
import com.social.account.AccountRepository;
import com.social.account.UserRole;
import com.social.core.exception.ConflictException;
import com.social.core.exception.EntityNotFoundException;
import com.social.email.EmailSender;
import com.social.security.model.UserContext;
import com.social.security.model.token.JwtTokenFactory;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RegistrationService {

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RegistrationConverter registrationConverter;

    @Autowired
    private EmailSender emailSender;

    @Autowired
    private JwtTokenFactory tokenFactory;

    @Autowired
    private AccountRepository accountRepository;

    public LoginDTO register(Boolean test, RegistrationDTO registrationDTO) {

        accountRepository.findByEmail(registrationDTO.getEmail()).ifPresent(account -> {
            throw new ConflictException("Account with same username exists");
        });

        Registration registration = registrationConverter.toEntity(registrationDTO);
        registration.setPassword(passwordEncoder.encode(registration.getPassword()));
        registration.setToken(RandomStringUtils.randomAlphanumeric(40));
        registrationRepository.save(registration);

        if (test) {
            return confirmRegister(registration.getToken());
        } else {
            emailSender.sendRegistrationToken(registration.getEmail(), registration.getToken());
        }
        return null;
    }

    public LoginDTO confirmRegister(String token) {

        LoginDTO dto = new LoginDTO();
        dto.setFirstLogin(true);
        Registration registration = registrationRepository.findByToken(token);

        accountRepository.findByEmail(registration.getEmail()).ifPresent(account -> {
            throw new ConflictException("Account with same username exists");
        });

        if (registration == null) {
            throw new EntityNotFoundException("No token found");
        }
        final List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("USER"));

        Account account = createAccount(registration);

        UserContext userContext = UserContext.create(registration.getEmail(), account.getUuid().toString(), authorities);
        userContext.setId(account.getId());
        dto.setToken(tokenFactory.createAccessJwtToken(userContext).getToken());
        dto.setRefreshToken(tokenFactory.createRefreshToken(userContext).getToken());

        return dto;
    }

    private Account createAccount(Registration registration) {
        Account account = new Account();
        account.setActive(true);
        account.setFirstLogin(true);
        account.setFirstName(registration.getFirstName());
        account.setLastName(registration.getLastName());
        account.setEmail(registration.getEmail());
        account.setUserRole(UserRole.USER);
        account.setPassword(registration.getPassword());

        accountRepository.save(account);

        return account;
    }
}
