package com.social.registration;

import org.springframework.stereotype.Component;

@Component
public class RegistrationConverter {

    public Registration toEntity(RegistrationDTO registrationDTO){
        Registration registration = new Registration();
        registration.setFirstName(registrationDTO.getFirstName());
        registration.setLastName(registrationDTO.getLastName());
        registration.setEmail(registrationDTO.getEmail());
        registration.setPassword(registrationDTO.getPassword());
        return registration;
    }
}
