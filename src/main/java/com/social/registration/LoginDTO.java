package com.social.registration;

import lombok.Data;

@Data
public class LoginDTO {

    private boolean firstLogin;
    private String token;
    private String refreshToken;
}
