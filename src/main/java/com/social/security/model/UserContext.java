package com.social.security.model;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

@Data
public class UserContext {

    private Integer id;
    private Boolean firstLogin;

    private final String username;
    private final String uuid;
    private final List<GrantedAuthority> authorities;

    private UserContext(final String username, String uuid, final List<GrantedAuthority> authorities) {
        this.username = username;
        this.uuid = uuid;
        this.authorities = authorities;
    }

    public static UserContext create(
            final String username,
            String uuid,
            final List<GrantedAuthority> authorities) {
        if (StringUtils.isBlank(username)) {
            throw new IllegalArgumentException("Username is blank: " + username);
        }
        return new UserContext(username, uuid, authorities);
    }

    public Boolean isFirstLogin() {
        return firstLogin;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setFirstLogin(Boolean firstLogin) {
        this.firstLogin = firstLogin;
    }
}
