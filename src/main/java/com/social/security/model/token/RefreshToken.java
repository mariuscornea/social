package com.social.security.model.token;

import com.social.security.model.Scopes;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;

import java.util.List;
import java.util.Optional;

public class RefreshToken implements JwtToken {
	private final Jws<Claims> claims;

	private RefreshToken(final Jws<Claims> claims) {
		this.claims = claims;
	}

	public static Optional<RefreshToken> create(
			final RawAccessJwtToken token,
			final String signingKey) {
		final Jws<Claims> claims = token.parseClaims(signingKey);

		final List<String> scopes = claims.getBody().get("scopes", List.class);
		if (scopes == null || scopes.isEmpty() || !scopes.stream().filter(scope -> Scopes.REFRESH_TOKEN.authority().equals(scope)).findFirst().isPresent()) {
			return Optional.empty();
		}

		return Optional.of(new RefreshToken(claims));
	}

	@Override
	public String getToken() {
		return null;
	}

	public Jws<Claims> getClaims() {
		return this.claims;
	}

	public String getJti() {
		return this.claims.getBody().getId();
	}

	public String getSubject() {
		return this.claims.getBody().getSubject();
	}
}
