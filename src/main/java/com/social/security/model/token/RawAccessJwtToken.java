package com.social.security.model.token;

import com.social.security.exceptions.JwtExpiredTokenException;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RawAccessJwtToken implements JwtToken {
	private static Logger logger	= LoggerFactory.getLogger(RawAccessJwtToken.class);

	private final String	token;

	public RawAccessJwtToken(final String token) {
		this.token = token;
	}

	public Jws<Claims> parseClaims(
			final String signingKey) {
		try {
			return Jwts.parser().setSigningKey(signingKey).parseClaimsJws(this.token);
		} catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException | SignatureException ex) {
			logger.error("Invalid JWT Token", ex);
			throw new JwtExpiredTokenException(this, "Invalid JWT token: ", ex); //TODO throw bad token
//			throw new InvalidTokenException("Invalid JWT token: ", ex);
		} catch (final ExpiredJwtException expiredEx) {
			logger.info("JWT Token is expired", expiredEx);
			throw new JwtExpiredTokenException(this, "JWT Token expired", expiredEx);
		}
	}

	@Override
	public String getToken() {
		return this.token;
	}
}
