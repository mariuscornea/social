package com.social.security.model.token;

public interface JwtToken {
    String getToken();
}
