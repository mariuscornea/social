package com.social.security.model.token;

import com.social.security.config.JwtSettings;
import com.social.security.model.Scopes;
import com.social.security.model.UserContext;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class JwtTokenFactory {
    private final JwtSettings settings;

    @Autowired
    public JwtTokenFactory(final JwtSettings settings) {
        this.settings = settings;
    }

    public AccessJwtToken createAccessJwtToken(
            final UserContext userContext) {
        if (StringUtils.isBlank(userContext.getUsername())) {
            throw new IllegalArgumentException("Cannot create JWT Token without username");
        }

        if (userContext.getAuthorities() == null || userContext.getAuthorities().isEmpty()) {
            throw new IllegalArgumentException("User doesn't have any privileges");
        }

        final Claims claims = Jwts.claims().setSubject(userContext.getUsername());
        claims.put("scopes", userContext.getAuthorities().stream().map(s -> s.toString()).collect(Collectors.toList()));
        claims.put("id", userContext.getId());
        claims.put("uuid", userContext.getUuid());
        final Date currentTime = Date.from(Instant.now());

        final String token = Jwts.builder()
                .setClaims(claims)
                .setIssuer(this.settings.getTokenIssuer())
                .setIssuedAt(currentTime)
                .setExpiration(Date.from(Instant.ofEpochMilli(currentTime.getTime() + this.settings.getTokenExpirationTime())))
                .signWith(SignatureAlgorithm.HS512, this.settings.getTokenSigningKey())
                .compact();
        return new AccessJwtToken(token, claims);
    }

    public JwtToken createRefreshToken(
            final UserContext userContext) {
        if (StringUtils.isBlank(userContext.getUsername())) {
            throw new IllegalArgumentException("Cannot create JWT Token without username");
        }

        final Date currentTime = Date.from(Instant.now());

        final Claims claims = Jwts.claims().setSubject(userContext.getUsername());
        claims.put("scopes", Arrays.asList(Scopes.REFRESH_TOKEN.authority()));
        claims.put("id", userContext.getId());
        claims.put("uuid", userContext.getUuid());
        // @formatter:off
        final String token = Jwts.builder()
                .setClaims(claims)
                .setIssuer(this.settings.getTokenIssuer())
                .setId(UUID.randomUUID().toString())
                .setIssuedAt(currentTime)
                .setExpiration(Date.from(Instant.ofEpochMilli(currentTime.getTime() + this.settings.getRefreshTokenExpTime())))
                .signWith(SignatureAlgorithm.HS512, this.settings.getTokenSigningKey())
                .compact();
        // @formatter:on

        return new AccessJwtToken(token, claims);
    }
}
