package com.social.security.auth.jwt;

import com.social.security.auth.JwtAuthenticationToken;
import com.social.security.config.JwtSettings;
import com.social.security.model.UserContext;
import com.social.security.model.token.RawAccessJwtToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {
	private final JwtSettings jwtSettings;

	@Autowired
	public JwtAuthenticationProvider(final JwtSettings jwtSettings) {
		this.jwtSettings = jwtSettings;
	}

	@Override
	public Authentication authenticate(
			final Authentication authentication) throws AuthenticationException {
		final RawAccessJwtToken rawAccessToken = (RawAccessJwtToken) authentication.getCredentials();
		final Jws<Claims> jwsClaims = rawAccessToken.parseClaims(this.jwtSettings.getTokenSigningKey());

		final String subject = jwsClaims.getBody().getSubject();
		final Integer id = (Integer) jwsClaims.getBody().get("id");
		final String uuid = (String) jwsClaims.getBody().get("uuid");

		final List<String> scopes = jwsClaims.getBody().get("scopes", List.class);
		final List<GrantedAuthority> authorities = scopes.stream().map(authority -> new SimpleGrantedAuthority(authority)).collect(Collectors.toList());
		final UserContext context = UserContext.create(subject, uuid, authorities);
		context.setId(id);
		return new JwtAuthenticationToken(context, context.getAuthorities());
	}

	@Override
	public boolean supports(
			final Class<?> authentication) {
		return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
	}
}
