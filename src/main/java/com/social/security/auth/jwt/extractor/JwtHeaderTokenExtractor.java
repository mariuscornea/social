package com.social.security.auth.jwt.extractor;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.stereotype.Component;

@Component
public class JwtHeaderTokenExtractor implements TokenExtractor {
	public static String HEADER_PREFIX = "Bearer ";

	@Override
	public String extract(
			final String header) {
		if (StringUtils.isBlank(header)) {
			throw new AuthenticationServiceException("Authorization header cannot be blank!");
		}

		if (header.startsWith(HEADER_PREFIX)) {
			return header.substring(HEADER_PREFIX.length(), header.length());
		}

		return header.substring(0, header.length());
	}
}
