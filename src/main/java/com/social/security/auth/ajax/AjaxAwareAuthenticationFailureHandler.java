package com.social.security.auth.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.social.core.exception.core.ErrorInfo;
import com.social.security.exceptions.AuthMethodNotSupportedException;
import com.social.security.exceptions.JwtExpiredTokenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

@Component
public class AjaxAwareAuthenticationFailureHandler implements AuthenticationFailureHandler {
	private final ObjectMapper mapper;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	public AjaxAwareAuthenticationFailureHandler(final ObjectMapper mapper) {
		this.mapper = mapper;
	}

	@Override
	public void onAuthenticationFailure(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final AuthenticationException e) throws IOException, ServletException {

		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		response.setContentType(MediaType.APPLICATION_JSON_VALUE); // TODO
		// System.out.println(e.toString());
		if (e instanceof BadCredentialsException || e instanceof UsernameNotFoundException) {
			this.mapper.writeValue(response.getWriter(),
					ErrorInfo.builder().status(HttpStatus.UNAUTHORIZED.value()).messageKey("e")
							.message(this.messageSource.getMessage("e", null, e.getMessage(), Locale.ENGLISH))
							.developerMessage(e.getMessage()).build());
		} else if (e instanceof JwtExpiredTokenException) {
			this.mapper.writeValue(response.getWriter(),
					ErrorInfo.builder().status(HttpStatus.UNAUTHORIZED.value()).messageKey("e")
							.message(this.messageSource.getMessage("e", null, e.getMessage(), Locale.ENGLISH))
							.developerMessage(e.getMessage()).build());
		} else if (e instanceof AuthMethodNotSupportedException) {
			this.mapper.writeValue(response.getWriter(),
					ErrorInfo.builder().status(HttpStatus.UNAUTHORIZED.value()).messageKey("e")
							.message(this.messageSource.getMessage("e", null, e.getMessage(), Locale.ENGLISH))
							.developerMessage(e.getMessage()).build());
		} else if (e instanceof AuthenticationServiceException) {
			this.mapper.writeValue(response.getWriter(),
					ErrorInfo.builder().status(HttpStatus.UNAUTHORIZED.value()).messageKey("e")
							.message(this.messageSource.getMessage("e", null, e.getMessage(), Locale.ENGLISH))
							.developerMessage(e.getMessage()).build());
		} else {
			this.mapper.writeValue(response.getWriter(),
					ErrorInfo.builder().status(HttpStatus.UNAUTHORIZED.value()).messageKey("e")
							.message(this.messageSource.getMessage("Another exception!!!", null, e.getMessage(), Locale.ENGLISH))
							.developerMessage(e.getMessage()).build());
		}

	}
}
