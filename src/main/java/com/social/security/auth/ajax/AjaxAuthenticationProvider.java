package com.social.security.auth.ajax;

import com.social.account.Account;
import com.social.account.AccountService;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

@Component
public class AjaxAuthenticationProvider implements AuthenticationProvider {
	private final BCryptPasswordEncoder	encoder;
	private final AccountService accountService;

	@Autowired
	public AjaxAuthenticationProvider(final AccountService accountService, final BCryptPasswordEncoder encoder) {
		this.accountService = accountService;
		this.encoder = encoder;
	}

	@Override
	public Authentication authenticate(
			final Authentication authentication) throws AuthenticationException {
		Assert.notNull(authentication, "No authentication data provided");

		final String username = (String) authentication.getPrincipal();
		final String password = (String) authentication.getCredentials();

		final Account account = this.accountService.findByEmail(username).orElseThrow(() -> new BadCredentialsException("Incorrect user/password"));

		if(!encoder.matches(password, account.getPassword())){
			throw new BadCredentialsException("Incorrect user/password");
		}

		if (account.getUserRole() == null) {
			throw new InsufficientAuthenticationException("User has no roles assigned");
		}

		final List<GrantedAuthority> authorities = new ArrayList<>();

		authorities.add(new SimpleGrantedAuthority(account.getUserRole().toString()));

		final UserContext userContext = UserContext.create(account.getEmail(), account.getUuid().toString(), authorities);
		userContext.setFirstLogin(account.getFirstLogin());
		userContext.setId(account.getId());

		return new UsernamePasswordAuthenticationToken(userContext, null, userContext.getAuthorities());
	}

	@Override
	public boolean supports(
			final Class<?> authentication) {
		return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}
}
