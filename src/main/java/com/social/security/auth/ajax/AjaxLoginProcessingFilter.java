package com.social.security.auth.ajax;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.social.security.exceptions.AuthMethodNotSupportedException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AjaxLoginProcessingFilter extends AbstractAuthenticationProcessingFilter {
	private static Logger logger	= LoggerFactory.getLogger(AjaxLoginProcessingFilter.class);

	private final AuthenticationSuccessHandler	successHandler;
	private final AuthenticationFailureHandler	failureHandler;

	private final ObjectMapper objectMapper;

	public AjaxLoginProcessingFilter(final String defaultProcessUrl, final AuthenticationSuccessHandler successHandler,
			final AuthenticationFailureHandler failureHandler, final ObjectMapper mapper) {
		super(defaultProcessUrl);
		this.successHandler = successHandler;
		this.failureHandler = failureHandler;
		this.objectMapper = mapper;
	}

	@Override
	public Authentication attemptAuthentication(
			final HttpServletRequest request,
			final HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
		if (!HttpMethod.POST.name().equals(request.getMethod())) {
			if (HttpMethod.OPTIONS.name().equals(request.getMethod())) {
				response.setStatus(HttpServletResponse.SC_OK);
				return null;
			} else {
				throw new AuthMethodNotSupportedException("Authentication method not supported");
			}
		}
		final LoginRequest loginRequest = this.objectMapper.readValue(request.getReader(), LoginRequest.class);

		if (StringUtils.isBlank(loginRequest.getUsername()) || StringUtils.isBlank(loginRequest.getPassword())) {
			throw new AuthenticationServiceException("Username or Password not provided");
		}

		final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword());

		response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin") == null ? "http://localhost:4200"
				: request.getHeader("Origin").isEmpty() ? "http://localhost:4200" : request.getHeader("Origin"));

		return this.getAuthenticationManager().authenticate(token);
	}

	@Override
	protected void successfulAuthentication(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final FilterChain chain,
			final Authentication authResult) throws IOException, ServletException {
		this.successHandler.onAuthenticationSuccess(request, response, authResult);
	}

	@Override
	protected void unsuccessfulAuthentication(
			final HttpServletRequest request,
			final HttpServletResponse response,
			final AuthenticationException failed) throws IOException, ServletException {
		SecurityContextHolder.clearContext();
		this.failureHandler.onAuthenticationFailure(request, response, failed);
	}
}
