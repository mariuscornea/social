package com.social.security;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException ex)
			throws IOException, ServletException {
//		response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin") == null ? "http://localhost:4200"
//				: request.getHeader("Origin").isEmpty() ? "http://localhost:4200" : request.getHeader("Origin"));
		response.sendError(HttpStatus.UNAUTHORIZED.value(), "Unauthorized");
	}
}
