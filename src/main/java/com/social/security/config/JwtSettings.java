package com.social.security.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "security.jwt")
public class JwtSettings {

	private Long	tokenExpirationTime;
	private String	tokenIssuer;
	private String	tokenSigningKey;
	private Long	refreshTokenExpTime;

	public Long getRefreshTokenExpTime() {
		return this.refreshTokenExpTime;
	}

	public void setRefreshTokenExpTime(
			final Long refreshTokenExpTime) {
		this.refreshTokenExpTime = refreshTokenExpTime;
	}

	public Long getTokenExpirationTime() {
		return this.tokenExpirationTime;
	}

	public void setTokenExpirationTime(
			final Long tokenExpirationTime) {
		this.tokenExpirationTime = tokenExpirationTime;
	}

	public String getTokenIssuer() {
		return this.tokenIssuer;
	}

	public void setTokenIssuer(
			final String tokenIssuer) {
		this.tokenIssuer = tokenIssuer;
	}

	public String getTokenSigningKey() {
		return this.tokenSigningKey;
	}

	public void setTokenSigningKey(
			final String tokenSigningKey) {
		this.tokenSigningKey = tokenSigningKey;
	}
}
