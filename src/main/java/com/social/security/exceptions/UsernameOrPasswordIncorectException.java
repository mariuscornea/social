package com.social.security.exceptions;


import com.social.core.exception.core.LocalizableRuntimeException;

public class UsernameOrPasswordIncorectException extends LocalizableRuntimeException {

	private static final long	serialVersionUID	= -6835311422020585887L;

	private Class<?>			entityClass;

	public UsernameOrPasswordIncorectException(final String message) {
		super(message);
	}

	public UsernameOrPasswordIncorectException(final String message, final String messageKey, final Object... messageArguments) {
		super(message, messageKey, messageArguments);
	}

	public UsernameOrPasswordIncorectException(final Class<?> entityClass, final String message) {
		super(message);
		this.entityClass = entityClass;
	}

	public Class<?> getEntityClass() {
		return this.entityClass;
	}
}
