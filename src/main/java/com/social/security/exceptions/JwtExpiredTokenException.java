package com.social.security.exceptions;

import com.social.security.model.token.JwtToken;
import org.springframework.security.core.AuthenticationException;

public class JwtExpiredTokenException extends AuthenticationException {
	private static final long	serialVersionUID	= -5959543783324224864L;

	private JwtToken token;

	public JwtExpiredTokenException(final String msg) {
		super(msg);
	}

	public JwtExpiredTokenException(final JwtToken token, final String msg, final Throwable t) {
		super(msg, t);
		this.token = token;
	}

	public String token() {
		return this.token.getToken();
	}
}
