package com.social.security.endpoint;

import com.social.account.Account;
import com.social.account.AccountService;
import com.social.security.auth.jwt.extractor.TokenExtractor;
import com.social.security.config.JwtSettings;
import com.social.security.config.WebSecurityConfig;
import com.social.security.exceptions.InvalidJwtToken;
import com.social.security.model.UserContext;
import com.social.security.model.token.JwtToken;
import com.social.security.model.token.JwtTokenFactory;
import com.social.security.model.token.RawAccessJwtToken;
import com.social.security.model.token.RefreshToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class RefreshTokenEndpoint {
	@Autowired
	private JwtTokenFactory tokenFactory;
	@Autowired
	private JwtSettings jwtSettings;
	@Autowired
	private AccountService accountService;

	@Autowired
	@Qualifier("jwtHeaderTokenExtractor")
	private TokenExtractor tokenExtractor;

	@RequestMapping(value = "/api/auth/token", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public @ResponseBody
	JwtToken refreshToken(
			final HttpServletRequest request,
			final HttpServletResponse response) throws IOException, ServletException {
		final String tokenPayload = this.tokenExtractor.extract(request.getHeader(WebSecurityConfig.JWT_TOKEN_HEADER_PARAM));

		final RawAccessJwtToken rawToken = new RawAccessJwtToken(tokenPayload);
		final RefreshToken refreshToken = RefreshToken.create(rawToken, this.jwtSettings.getTokenSigningKey()).orElseThrow(() -> new InvalidJwtToken());

		final String jti = refreshToken.getJti();

		// TODO check account role
		final String subject = refreshToken.getSubject();
		final Account account = this.accountService.findByEmail(subject).orElseThrow(() -> new UsernameNotFoundException("User not found: " + subject));

		final List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		authorities.add(new SimpleGrantedAuthority(account.getUserRole().toString()));

		final UserContext userContext = UserContext.create(account.getEmail(), account.getUuid().toString(), authorities);

		return this.tokenFactory.createAccessJwtToken(userContext);
	}
}
