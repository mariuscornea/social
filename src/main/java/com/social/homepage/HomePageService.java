package com.social.homepage;

import com.social.business.Business;
import com.social.business.BusinessRepository;
import com.social.homepage.dto.HomePageDTO;
import com.social.offer.Offer;
import com.social.offer.OfferRepository;
import com.social.project.Project;
import com.social.project.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class HomePageService {

    @Autowired
    private BusinessRepository businessRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private OfferRepository offerRepository;
    @Autowired
    private HomePageConverter homePageConverter;

    public HomePageDTO homePageTop10(){
        List<Business> businesses = businessRepository.findTop10ByOrderByPublicationDateDesc();
        List<Project> projects = projectRepository.findTop10ByOrderByPublicationDateDesc();
        List<Offer> offers = offerRepository.findTop10ByOrderByPublicationDateDesc();
        return homePageConverter.homePageDTO(businesses,projects,offers);
    }

}
