package com.social.homepage;

import com.social.homepage.dto.HomePageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class HomePageController {

    @Autowired
    private HomePageService homePageService;

    @RequestMapping(value = "/top-10-home-page", method = RequestMethod.GET)
    public HomePageDTO getTop10BusinessesProjectsAndOffers() {
        return homePageService.homePageTop10();
    }
}
