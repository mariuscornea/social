package com.social.homepage.dto;

import com.social.business.dto.BusinessDTO;
import com.social.offer.dto.OfferDTO;
import com.social.project.dto.ProjectDTO;
import lombok.Data;

import java.util.List;

@Data
public class HomePageDTO {

    private List<BusinessDTO> businesses;
    private List<ProjectDTO> projects;
    private List<OfferDTO> offers;
}
