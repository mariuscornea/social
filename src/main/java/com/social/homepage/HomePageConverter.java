package com.social.homepage;

import com.social.business.Business;
import com.social.business.BusinessConverter;
import com.social.homepage.dto.HomePageDTO;
import com.social.offer.Offer;
import com.social.offer.OfferConverter;
import com.social.project.Project;
import com.social.project.ProjectConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class HomePageConverter {

    @Autowired
    private BusinessConverter businessConverter;
    @Autowired
    private ProjectConverter projectConverter;
    @Autowired
    private OfferConverter offerConverter;

    public HomePageDTO homePageDTO(List<Business> businesses, List<Project> projects, List<Offer> offers){
        HomePageDTO homePageDTO = new HomePageDTO();
        homePageDTO.setBusinesses(businessConverter.toDTOList(businesses));
        homePageDTO.setProjects(projectConverter.toDTOList(projects));
        homePageDTO.setOffers(offerConverter.toDTOList(offers));
        return homePageDTO;
    }
}
