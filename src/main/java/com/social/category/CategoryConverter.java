package com.social.category;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CategoryConverter {

    public CategoryDTO toDTO(Category category){
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(category.getId());
        categoryDTO.setName(category.getName());
        categoryDTO.setSrcLogo(category.getSrcLogo());

        return categoryDTO;
    }

    public List<CategoryDTO> toDTOList(List<Category> categories){
        return categories.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
