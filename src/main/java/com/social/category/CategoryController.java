package com.social.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    public List<CategoryDTO> findAll() {
        return categoryService.findAll();
    }

    @RequestMapping(value = "/categories/top", method = RequestMethod.GET)
    public List<CategoryDTO> findTop12() {
        return categoryService.findTop12();
    }
}
