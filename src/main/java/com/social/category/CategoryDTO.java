package com.social.category;

import lombok.Data;

@Data
public class CategoryDTO {

    private Integer id;
    private String name;
    private String srcLogo;
}
