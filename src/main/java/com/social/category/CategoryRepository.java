package com.social.category;

import com.social.core.repo.BaseJpaRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends BaseJpaRepository<Category, Integer> {

    Optional<Category> findByName(String name);

    List<Category> findTop12ByOrderByName();
}
