package com.social.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoryConverter categoryConverter;

    public List<CategoryDTO> findAll(){
        return categoryConverter.toDTOList(categoryRepository.findAll());
    }

    public List<CategoryDTO> findTop12(){
        return categoryConverter.toDTOList(categoryRepository.findTop12ByOrderByName());
    }
}
