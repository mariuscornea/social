package com.social.tags;

import com.social.core.exception.EntityNotFoundException;
import com.social.tags.benefit.BenefitTagRepository;
import com.social.tags.product.ProductTagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TagUtils {

    @Autowired
    private BenefitTagRepository benefitTagRepository;

    @Autowired
    private ProductTagRepository productTagRepository;

    public String checkBenefitTags(List<String> benefitTags) {
        return benefitTags.stream().map(benefitTag -> benefitTagRepository.findByName(benefitTag)
                .orElseThrow(() -> new EntityNotFoundException("BenefitTag not found by name:" + benefitTag)))
                .map(benefitTag -> benefitTag.getName()).collect(Collectors.joining(" "));
    }

    public String checkProductTags(List<String> productTags) {
        return productTags.stream().map(productTag -> productTagRepository.findByName(productTag)
                .orElseThrow(() -> new EntityNotFoundException("ProductTag not found by name:" + productTag)))
                .map(productTag -> productTag.getName()).collect(Collectors.joining(" "));
    }
}
