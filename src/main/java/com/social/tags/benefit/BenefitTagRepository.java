package com.social.tags.benefit;

import com.social.core.repo.BaseJpaRepository;

import java.util.Optional;

public interface BenefitTagRepository extends BaseJpaRepository<BenefitTag, Integer>{

    Optional<BenefitTag> findByName(String name);
}
