package com.social.tags.benefit;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class BenefitTagConverter {

    public List<String> toList(String benefitTags) {
        return Stream.of(benefitTags.split(" "))
                .map(benefitTag -> new String(benefitTag))
                .collect(Collectors.toList());
    }

    public List<String> toList(List<BenefitTag> benefitTags) {
        return benefitTags.stream().map(benefitTag -> benefitTag.getName()).collect(Collectors.toList());
    }
}
