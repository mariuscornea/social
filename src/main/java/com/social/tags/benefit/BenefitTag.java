package com.social.tags.benefit;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class BenefitTag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @Column(nullable = false)
    private String name;
}
