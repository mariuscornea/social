package com.social.tags.product;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class ProductTagConverter {

    public List<String> toList(String productTags) {
        return Stream.of(productTags.split(" "))
                .map(productTag -> new String(productTag))
                .collect(Collectors.toList());
    }

    public List<String> toList(List<ProductTag> productTags) {
        return productTags.stream().map(productTag -> productTag.getName()).collect(Collectors.toList());
    }
}
