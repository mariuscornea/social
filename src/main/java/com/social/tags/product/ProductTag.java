package com.social.tags.product;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class ProductTag {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @Column(nullable = false)
    private String name;
}
