package com.social.tags.product;

import com.social.core.repo.BaseJpaRepository;

import java.util.Optional;

public interface ProductTagRepository extends BaseJpaRepository<ProductTag, Integer>{

    Optional<ProductTag> findByName(String name);
}
