package com.social.test;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class TestIndex {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @Column(nullable = false)
    private String data1;

    @Column(nullable = false)
    private String data2;

    @Column(nullable = false)
    private String data3;

    @Column(nullable = false)
    private String data4;

    @Column(nullable = false)
    private String data5;

    @Column(nullable = false)
    private String data6;

    @Column(nullable = false)
    private String data7;

    @Column(nullable = false)
    private String data8;

    @Column(nullable = false)
    private String data9;

    @Column(nullable = false)
    private String data10;

    @Column(nullable = false)
    private String data11;

    @Column(nullable = false)
    private String data12;
}
