package com.social.test;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@RestController
public class TestFullSearchController {

    @Autowired
    private TestFullSearchRepository testFullSearchRepository;

    @Autowired
    private TestIndexRepository testIndexRepository;

    private static final Random random = new Random();

    @RequestMapping(value = "/api/ping/{id}", method = RequestMethod.GET)
    public void ping(@PathVariable("id") Integer id) {
        System.out.println(id);
    }

    @RequestMapping(value = "/api/test", method = RequestMethod.POST)
    public void test(@RequestBody TestFullSearch testFullSearch1) {

        for (int i = 0; i < 10_000; i++) {
            String phrase = "";
            for (int j = 0; j < random.nextInt(8) + 1; j++) {
                phrase += RandomStringUtils.randomAlphabetic(random.nextInt(15) + 1) + " ";
            }
            phrase += RandomStringUtils.randomAlphabetic(random.nextInt(15) + 1);
            TestFullSearch testFullSearch = new TestFullSearch();
            testFullSearch.setFullText(phrase);
            testFullSearchRepository.save(testFullSearch);
        }
    }

    @RequestMapping(value = "/api/test1", method = RequestMethod.POST)
    public void test1() {
        int i = 0;
        List<TestFullSearch> testFullSearches = testFullSearchRepository.findAll();
        for (TestFullSearch testFullSearch : testFullSearches) {
            testFullSearch.setIndex(random.nextInt(400));
            i++;
            if (i % 10 == 0) {
                testFullSearch.setFullText(testFullSearch.getFullText() + " marius");
            }
            testFullSearchRepository.save(testFullSearch);
        }
//        TestIndex testIndex = testIndexRepository.findOne(566565);
//        testIndex.setData1(RandomStringUtils.randomAlphanumeric(32).toLowerCase());
//        testIndex.setData2(RandomStringUtils.randomAlphanumeric(32).toLowerCase());
//        testIndex.setData3(RandomStringUtils.randomAlphanumeric(32).toLowerCase());
//        testIndex.setData4(RandomStringUtils.randomAlphanumeric(32).toLowerCase());
//        testIndex.setData5(RandomStringUtils.randomAlphanumeric(32).toLowerCase());
//        testIndex.setData6(RandomStringUtils.randomAlphanumeric(32).toLowerCase());
//        testIndex.setData7(RandomStringUtils.randomAlphanumeric(32).toLowerCase());
//        testIndex.setData8(RandomStringUtils.randomAlphanumeric(32).toLowerCase());
//        testIndex.setData9(RandomStringUtils.randomAlphanumeric(32).toLowerCase());
//        testIndex.setData10(RandomStringUtils.randomAlphanumeric(32).toLowerCase());
//        testIndex.setData11(RandomStringUtils.randomAlphanumeric(32).toLowerCase());
//        testIndex.setData12(RandomStringUtils.randomAlphanumeric(32).toLowerCase());
//        testIndexRepository.save(testIndex);
    }

    public static void main(String[] args) {

//        String s = RandomStringUtils.randomAlphabetic(32);
//        System.out.println(s);
//        s = RandomStringUtils.randomAlphabetic(32);
//        System.out.println(s);
//        s = RandomStringUtils.randomAlphabetic(32);
//        System.out.println(s);
//        s = RandomStringUtils.randomAlphabetic(32);
//        System.out.println(s);
//        s = RandomStringUtils.randomAlphabetic(32);
//        System.out.println(s);
//        s = RandomStringUtils.randomAlphabetic(32);
//        System.out.println(s);
//        s = RandomStringUtils.randomAlphabetic(32);
//        System.out.println(s);
//        s = RandomStringUtils.randomAlphabetic(32);
//        System.out.println(s);

    }
}
