package com.social.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.persistence.PostPersist;
import javax.persistence.PostUpdate;

@Component
public class TestFullSearchListener {

    private static TestFullSearchRepository testFullSearchRepository;

    @PostPersist
    public void testFullSearchPostPersist(TestFullSearch testFullSearch) {
        testFullSearchRepository.test(testFullSearch.getId());
    }

    @PostUpdate
    public void testFullSearchPostUpdate(TestFullSearch testFullSearch) {
        System.out.println(testFullSearch);
    }

    @Autowired
    @Qualifier("testFullSearchRepository")
    public void setTestFullSearchRepository(TestFullSearchRepository testFullSearchRepository)
    {
        this.testFullSearchRepository = testFullSearchRepository;
    }
}
