package com.social.test;

import com.social.core.repo.BaseRepository;

public class TestFullSearchRepositoryImpl extends BaseRepository implements TestFullSearchRepositoryCustom {

    @Override
    public void test(Integer id) {
        jdbcTemplate.update("UPDATE test_full_search SET full_text_tokens = to_tsvector(full_text) WHERE id = " + id);
    }

}
