package com.social.test;

import com.social.core.repo.BaseJpaRepository;
import org.springframework.beans.factory.annotation.Qualifier;

@Qualifier("testFullSearchRepository")
public interface TestFullSearchRepository extends BaseJpaRepository<TestFullSearch, Integer>, TestFullSearchRepositoryCustom {
}
