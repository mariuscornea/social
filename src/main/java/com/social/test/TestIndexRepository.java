package com.social.test;

import com.social.core.repo.BaseJpaRepository;

public interface TestIndexRepository extends BaseJpaRepository<TestIndex, Integer> {

}
