package com.social.test;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.persistence.*;

@Data
@Entity
@EntityListeners(TestFullSearchListener.class)
@Component
public class TestFullSearch {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @Column(nullable = false)
    private String fullText;

    @Transient
    @Column(columnDefinition ="TSVECTOR", insertable = false)
    private String fullTextTokens;

    @Column
    private Integer index;
}
