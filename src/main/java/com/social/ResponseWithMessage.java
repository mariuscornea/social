package com.social;

import lombok.Data;

@Data
public class ResponseWithMessage<T> {

    private T data;
    private String message;

    public ResponseWithMessage(T data, String message) {
        this.data = data;
        this.message = message;
    }

    public ResponseWithMessage(String message) {
        this.data = null;
        this.message = message;
    }
}
