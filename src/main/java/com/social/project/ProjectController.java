package com.social.project;

import com.social.ResponseWithMessage;
import com.social.core.exception.FieldsException;
import com.social.project.dto.CreateProjectDTO;
import com.social.project.dto.ProjectDTO;
import com.social.project.dto.ProjectSearchDTO;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/auth/projects", method = RequestMethod.POST)
    public ResponseWithMessage<ProjectDTO> createProject(@AuthenticationPrincipal UserContext userContext,
                                                         @Valid @RequestBody CreateProjectDTO createProjectDTO,
                                                         BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Project fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage(projectService.createProject(userContext, createProjectDTO), "Success message");
    }

    @RequestMapping(value = "/auth/projects/{projectId}", method = RequestMethod.PUT)
    public ResponseWithMessage<ProjectDTO> editProject(@AuthenticationPrincipal UserContext userContext,
                                                       @PathVariable("projectId") Integer projectId,
                                                       @Valid @RequestBody CreateProjectDTO createProjectDTO,
                                                       BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Project fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage(projectService.editProject(userContext, projectId, createProjectDTO), "Success message");
    }

    @RequestMapping(value = "/auth/projects", method = RequestMethod.GET)
    public List<ProjectDTO> getAllForUser(@AuthenticationPrincipal UserContext userContext) {
        return projectService.findAllProjects(userContext);
    }

    @RequestMapping(value = "/auth/projects/{projectId}", method = RequestMethod.GET)
    public ProjectDTO getById(@PathVariable("projectId") Integer projectId) {
        return projectService.findById(projectId);
    }

    @RequestMapping(value = "/projects/search", method = RequestMethod.GET)
    public Page<ProjectSearchDTO> search(@RequestParam Map<String, String> parameters,
                                         @PageableDefault(page = 0, size = 20) Pageable pageable) {
        parameters.values().removeIf(x -> x.equals("null"));
        return projectService.searchProjects(parameters, pageable);
    }
}
