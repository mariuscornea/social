package com.social.project;

import com.social.account.Account;
import com.social.category.Category;
import com.social.project.file.ProjectFile;
import com.social.project.profession.ProjectProfession;
import com.social.util.enums.Currency;
import com.social.util.enums.ProjectType;
import com.vividsolutions.jts.geom.Geometry;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Account account;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Category category;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ProjectType projectType;

    @OneToMany(mappedBy = "project", fetch = FetchType.LAZY)
    private List<ProjectProfession> projectProfessions = new ArrayList<>();

    @Column(nullable = false)
    @Digits(integer=10, fraction=2)
    private BigDecimal budgetLowLimit;

    @Column(nullable = false)
    @Digits(integer=10, fraction=2)
    private BigDecimal budgetHighLimit;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Currency currency;

    @Column (columnDefinition = "geometry(Point,4326)")
    private Geometry geometry;

    @Column
    private Double latitude;

    @Column
    private Double longitude;

    @Column
    private String description;

    @Column
    private LocalDateTime projectDeadline;

    @Column
    private LocalDateTime publicationDate = LocalDateTime.now();

    @OneToMany(mappedBy = "project", fetch = FetchType.LAZY)
    private List<ProjectFile> projectFiles = new ArrayList<>();
}
