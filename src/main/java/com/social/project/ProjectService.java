package com.social.project;

import com.social.account.AccountRepository;
import com.social.category.Category;
import com.social.category.CategoryRepository;
import com.social.core.exception.EntityNotFoundException;
import com.social.file.FileService;
import com.social.project.dto.CreateProjectDTO;
import com.social.project.dto.ProjectDTO;
import com.social.project.dto.ProjectSearchDTO;
import com.social.project.file.ProjectFileRepository;
import com.social.project.profession.ProjectProfessionService;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service
@Transactional
public class ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private ProjectConverter projectConverter;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ProjectProfessionService projectProfessionService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProjectFileRepository projectFileRepository;

    @Autowired
    private FileService fileService;

    public ProjectDTO findById(Integer projectId) {
        return projectConverter.toDTO(projectRepository.findOne(projectId));
    }

    public List<ProjectDTO> findAllProjects(UserContext userContext) {
        return projectConverter.toDTOList(projectRepository.findByAccountIdOrderByIdDesc(userContext.getId()));
    }

    public Page<ProjectSearchDTO> searchProjects(Map<String, String> parameters, Pageable pageable) {
        return projectRepository.searchProjects(parameters, pageable);
    }

    public ProjectDTO createProject(UserContext userContext, CreateProjectDTO createProjectDTO) {
        Category category = categoryRepository.findByName(createProjectDTO.getCategoryName())
                .orElseThrow(() -> new EntityNotFoundException("Category not found by name:" + createProjectDTO.getCategoryName()));

        Project project = projectConverter.toEntity(createProjectDTO);
        project.setAccount(accountRepository.getOne(userContext.getId()));
        project.setCategory(category);

        projectRepository.save(project);

        createProjectDTO.getProfessions()
                .forEach(projectProfession -> project.getProjectProfessions().add(projectProfessionService.addProjectProfession(project, projectProfession)));

        projectRepository.save(project);

        return projectConverter.toDTO(project);
    }

    public ProjectDTO editProject(UserContext userContext, Integer projectId, CreateProjectDTO createProjectDTO) {
        Category category = categoryRepository.findByName(createProjectDTO.getCategoryName())
                .orElseThrow(() -> new EntityNotFoundException("Category not found by name:" + createProjectDTO.getCategoryName()));

        Project project = projectRepository.findByIdAndAccountId(projectId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Project not found by id:" + projectId));
        projectConverter.toEntity(project, createProjectDTO);
        project.setCategory(category);
        project.getProjectProfessions().clear();
        projectProfessionService.deleteAllProjects(projectId);

        createProjectDTO.getProfessions()
                .forEach(projectProfession -> project.getProjectProfessions().add(projectProfessionService.addProjectProfession(project, projectProfession)));

        projectRepository.saveAndFlush(project);

        return findById(projectId);
    }

//    public ProfilePhotoResponse addProjectFile(UserContext userContext, MultipartFile multipartFile) {
//        String photoSrc = fileService.saveFileToDisk(multipartFile);
////        account.setPhotoSrc(photoSrc);
////        accountRepository.save(account);
////
////        ProfilePhotoResponse profilePhotoResponse = new ProfilePhotoResponse();
////        profilePhotoResponse.setId(account.getId());
////        profilePhotoResponse.setPhotoSrc(account.getPhotoSrc());
//        return profilePhotoResponse;
//    }
}
