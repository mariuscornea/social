package com.social.project.profession;

import com.social.core.repo.BaseJpaRepository;

public interface ProjectProfessionRepository extends BaseJpaRepository<ProjectProfession, Integer>{

    void deleteByProjectId(Integer projectId);
}
