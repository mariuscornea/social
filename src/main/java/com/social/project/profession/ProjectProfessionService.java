package com.social.project.profession;

import com.social.account.miscellaneous.profession.ProfessionService;
import com.social.project.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProjectProfessionService {

    @Autowired
    private ProjectProfessionRepository projectProfessionRepository;

    @Autowired
    private ProfessionService professionService;

    public ProjectProfession addProjectProfession(Project project, String professionName){
        ProjectProfession projectProfession = new ProjectProfession();
        projectProfession.setProject(project);
        projectProfession.setProfession(professionService.addProfessionOrGetProfession(professionName));
        projectProfessionRepository.save(projectProfession);
        return projectProfession;
    }

    public void deleteAllProjects(Integer projectId){
        projectProfessionRepository.deleteByProjectId(projectId);
    }

}
