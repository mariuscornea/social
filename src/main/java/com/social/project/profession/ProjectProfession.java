package com.social.project.profession;

import com.social.account.miscellaneous.profession.Profession;
import com.social.project.Project;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class ProjectProfession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Project project;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Profession profession;
}
