package com.social.project.dto;

import com.social.util.enums.Currency;
import com.social.util.enums.ProjectType;
import lombok.Data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class ProjectSearchDTO {

    private Integer id;
    private String category;
    private String title;
    private ProjectType projectType;
    private List<String> professions = new ArrayList<>();
    private BigDecimal budgetLowLimit;
    private BigDecimal budgetHighLimit;
    private Currency currency;
    private Double latitude;
    private Double longitude;
    private String description;
    private Long projectDeadline;
}
