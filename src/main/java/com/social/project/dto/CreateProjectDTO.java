package com.social.project.dto;

import com.social.core.annotation.NotEmptyList;
import com.social.util.enums.Currency;
import com.social.util.enums.ProjectType;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
public class CreateProjectDTO {

    @NotNull
    private String categoryName;

    @NotNull
    private String title;

    @NotNull
    private ProjectType projectType;

    @NotEmptyList
    private List<String> professions = new ArrayList<>();

    @Digits(integer=10, fraction=2)
    private BigDecimal budgetLowLimit;

    @Digits(integer=10, fraction=2)
    private BigDecimal budgetHighLimit;

    @NotNull
    private Currency currency;

    @NotNull
    private Double latitude;

    @NotNull
    private Double longitude;

    @NotNull
    private String description;

    @NotNull
    private Long projectDeadline;
}
