package com.social.project;

import com.social.project.dto.CreateProjectDTO;
import com.social.project.dto.ProjectDTO;
import com.social.project.file.ProjectFile;
import com.social.project.file.dto.ProjectFileDTO;
import com.social.util.GeomUtil;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProjectConverter {

    public Project toEntity(CreateProjectDTO createProjectDTO) {
        Project project = new Project();
        project.setTitle(createProjectDTO.getTitle());
        project.setProjectType(createProjectDTO.getProjectType());
        project.setBudgetLowLimit(createProjectDTO.getBudgetLowLimit());
        project.setBudgetHighLimit(createProjectDTO.getBudgetHighLimit());
        project.setCurrency(createProjectDTO.getCurrency());
        project.setGeometry(GeomUtil.latLongToGeom(createProjectDTO.getLatitude(), createProjectDTO.getLongitude()));
        project.setLatitude(createProjectDTO.getLatitude());
        project.setLongitude(createProjectDTO.getLongitude());
        project.setDescription(createProjectDTO.getDescription());
        project.setProjectDeadline(LocalDateTime.ofInstant(Instant.ofEpochMilli(createProjectDTO.getProjectDeadline()), ZoneId.of("UTC")));
        return project;
    }

    public Project toEntity(Project project, CreateProjectDTO createProjectDTO) {
        project.setTitle(createProjectDTO.getTitle());
        project.setProjectType(createProjectDTO.getProjectType());
        project.setBudgetLowLimit(createProjectDTO.getBudgetLowLimit());
        project.setBudgetHighLimit(createProjectDTO.getBudgetHighLimit());
        project.setCurrency(createProjectDTO.getCurrency());
        project.setGeometry(GeomUtil.latLongToGeom(createProjectDTO.getLatitude(), createProjectDTO.getLongitude()));
        project.setLatitude(createProjectDTO.getLatitude());
        project.setLongitude(createProjectDTO.getLongitude());
        project.setDescription(createProjectDTO.getDescription());
        project.setProjectDeadline(LocalDateTime.ofInstant(Instant.ofEpochMilli(createProjectDTO.getProjectDeadline()), ZoneId.of("UTC")));
        return project;
    }

    public ProjectDTO toDTO(Project project) {
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setTitle(project.getTitle());
        projectDTO.setCategory(project.getCategory().getName());
        projectDTO.setProjectType(project.getProjectType());
        projectDTO.setBudgetLowLimit(project.getBudgetLowLimit());
        projectDTO.setBudgetHighLimit(project.getBudgetHighLimit());
        projectDTO.setLatitude(project.getLatitude());
        projectDTO.setLongitude(project.getLongitude());
        projectDTO.setDescription(project.getDescription());

        projectDTO.setProfessions(project.getProjectProfessions()
                .stream()
                .map(projectProfession -> projectProfession.getProfession().getName())
                .collect(Collectors.toList()));

        projectDTO.setProjectDeadline(project.getProjectDeadline() != null ? project.getProjectDeadline().toInstant(ZoneOffset.UTC).toEpochMilli() : null);
        projectDTO.setProjectFileDTOS(project.getProjectFiles().stream().map(this::toDTOListProjectFile).collect(Collectors.toList()));

        return projectDTO;
    }

    public List<ProjectDTO> toDTOList(List<Project> projects) {
        return projects.stream().map(this::toDTO).collect(Collectors.toList());
    }

    public ProjectFileDTO toDTOListProjectFile(ProjectFile projectFile) {
        ProjectFileDTO projectFileDTO = new ProjectFileDTO();
        projectFileDTO.setId(projectFile.getId());
        projectFileDTO.setName(projectFile.getName());
        return projectFileDTO;
    }
}
