package com.social.project.repo;

import com.social.core.repo.BaseRepository;
import com.social.core.sql.SQLQueryBuilder;
import com.social.pagination.PaginationRepo;
import com.social.project.dto.ProjectSearchDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class ProjectRepositoryImpl extends BaseRepository implements ProjectRepositoryCustom {

    private static final ProjectResultSetExtractor PROJECT_SEARCH_ROW_MAPPER = new ProjectResultSetExtractor();

    @Autowired
    private PaginationRepo<ProjectSearchDTO> paginationRepo;

    public Page<ProjectSearchDTO> searchProjects(Map<String, String> parameters, Pageable pageable) {
        SQLQueryBuilder queryBuilder = new SQLQueryBuilder()
                .select()
                .column("p.id", "id")
                .column("p.budget_low_limit", "budgetLowLimit")
                .column("p.budget_high_limit", "budgetHighLimit")
                .column("p.currency", "currency")
                .column("p.description", "description")
                .column("p.project_type", "projectType")
                .column("p.title", "title")
                .column("p.project_deadline", "projectDeadline")
                .column("p.latitude", "latitude")
                .column("p.longitude", "longitude")
                .column("c.name", "categoryName")
                .column("prof.name", "professionName")
                .from("project", "p")
                .innerJoinOn("category", "c", "p.category_id = c.id");
        if (parameters.containsKey("category")) {
            queryBuilder.append(" AND c.name = ?", parameters.get("category"));
        }
        queryBuilder.innerJoinOn("project_profession", "pp", "p.id = pp.project_id")
                .innerJoinOn("profession", "prof", "pp.profession_id = prof.id");


        queryBuilder.where().append("1 = 1");
        if (parameters.containsKey("name")) {
            queryBuilder.append(" AND p.title ILIKE ?", "%" + parameters.get("name") + "%");
        }
        if (parameters.containsKey("longitude") && parameters.containsKey("latitude")) {
            queryBuilder.append(" AND ST_DWithin(p.geometry, ST_GeographyFromText('SRID=4326; POINT(' || ? || ' ' || ? || ')'), 1000)", parameters.get("longitude"), parameters.get("latitude"));
        }

        return paginationRepo.getPaginatedResult(queryBuilder, PROJECT_SEARCH_ROW_MAPPER, pageable);
    }

}
