package com.social.project.repo;

import com.social.project.dto.ProjectSearchDTO;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ProjectResultSetExtractor implements ResultSetExtractor<List<ProjectSearchDTO>> {

    @Nullable
    @Override
    public List<ProjectSearchDTO> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        Map<Integer, ProjectSearchDTO> projectSearchDTOMap = new LinkedHashMap<>();

        while (resultSet.next()) {
            Integer id = resultSet.getInt("id");
            if (projectSearchDTOMap.containsKey(id)) {
                addProfessions(projectSearchDTOMap.get(id).getProfessions(), resultSet);
            } else {
                projectSearchDTOMap.put(id, getProjectSearchDTO(resultSet));
            }
        }

        return projectSearchDTOMap.values().stream().collect(Collectors.toList());
    }

    private ProjectSearchDTO getProjectSearchDTO(ResultSet resultSet) throws SQLException, DataAccessException {
        ProjectSearchDTO projectSearchDTO = new ProjectSearchDTO();

        projectSearchDTO.setId(resultSet.getInt("id"));
        projectSearchDTO.setCategory(resultSet.getString("categoryName"));
        projectSearchDTO.setTitle(resultSet.getString("title"));
        projectSearchDTO.setBudgetLowLimit(resultSet.getBigDecimal("budgetLowLimit"));
        projectSearchDTO.setBudgetHighLimit(resultSet.getBigDecimal("budgetHighLimit"));
        projectSearchDTO.setLatitude(resultSet.getDouble("latitude"));
        projectSearchDTO.setLongitude(resultSet.getDouble("longitude"));

        projectSearchDTO.getProfessions().add(resultSet.getString("professionName"));

        projectSearchDTO.setDescription(resultSet.getString("description"));

        return projectSearchDTO;

    }

    private void addProfessions(List<String> professions, ResultSet resultSet) throws SQLException {
        professions.add(resultSet.getString("professionName"));
    }
}
