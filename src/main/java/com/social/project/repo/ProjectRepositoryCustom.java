package com.social.project.repo;

import com.social.project.dto.ProjectSearchDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface ProjectRepositoryCustom {

    Page<ProjectSearchDTO> searchProjects(Map<String, String> parameters, Pageable pageable);
}
