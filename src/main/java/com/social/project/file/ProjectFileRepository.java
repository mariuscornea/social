package com.social.project.file;

import com.social.core.repo.BaseJpaRepository;

public interface ProjectFileRepository extends BaseJpaRepository<ProjectFile, Integer> {
}
