package com.social.project.file;

import com.social.project.Project;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class ProjectFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Project project;

    @Column(nullable = false)
    private String name;
}
