package com.social.project.file.dto;

import lombok.Data;

@Data
public class ProjectFileDTO {

    private Integer id;
    private String name;
}
