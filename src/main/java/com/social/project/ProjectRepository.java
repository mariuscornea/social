package com.social.project;

import com.social.core.repo.BaseJpaRepository;
import com.social.project.repo.ProjectRepositoryCustom;

import java.util.List;
import java.util.Optional;

public interface ProjectRepository extends BaseJpaRepository<Project, Integer>, ProjectRepositoryCustom {

    Optional<Project> findByIdAndAccountId(Integer projectId, Integer accountId);

    List<Project> findByAccountIdOrderByIdDesc(Integer accountId);

    List<Project> findTop10ByOrderByPublicationDateDesc();
}
