package com.social.template;

import com.social.business.Business;
import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Data
@Entity
public class Template {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Business business;

    @Column(nullable = false)
    private String name;

    @Column
    private String photoSrc1;

    @Column
    private String photoSrc2;

    @Column
    private String photoSrc3;

    @Column
    @Type(type="text")
    private String text1;

    @Column
    @Type(type="text")
    private String text2;

    @Column
    @Type(type="text")
    private String text3;

    @Column
    private Boolean selected = false;
}
