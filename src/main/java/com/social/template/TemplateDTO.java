package com.social.template;

import lombok.Data;

@Data
public class TemplateDTO {

    private String name;
    private String photoSrc1;
    private String photoSrc2;
    private String photoSrc3;
    private String text1;
    private String text2;
    private String text3;
}
