package com.social.template;

import com.social.core.repo.BaseJpaRepository;

import java.util.List;

public interface TemplateRepository extends BaseJpaRepository<Template, Integer> {

    Template findByBusinessIdAndName(Integer businessId, String name);

    List<Template>  findByBusinessId(Integer businessId);

}
