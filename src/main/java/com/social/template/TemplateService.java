package com.social.template;

import com.social.business.Business;
import com.social.business.BusinessRepository;
import com.social.file.FileService;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@Transactional
public class TemplateService {

    @Autowired
    private TemplateRepository templateRepository;

    @Autowired
    private TemplateConverter templateConverter;

    @Autowired
    private FileService fileService;

    @Autowired
    private BusinessRepository businessRepository;

    public TemplateDTO getTemplateByName(UserContext userContext, Integer businessId, String name) {
        Template template = findTemplate(userContext, businessId, name); //TODO change this to throw exception
        return templateConverter.toDTO(template);
    }

    public TemplateDTO addPicture1(UserContext userContext, Integer businessId, String name, MultipartFile multipartFile) {
        Template template = findTemplate(userContext, businessId, name);
        String photoSrc = fileService.saveFileToDisk(userContext.getUsername(), multipartFile);
        template.setPhotoSrc1(photoSrc);
        templateRepository.save(template);
        return templateConverter.toDTO(template);
    }

    public TemplateDTO addPicture2(UserContext userContext, Integer businessId, String name, MultipartFile multipartFile) {
        Template template = findTemplate(userContext, businessId, name);
        String photoSrc = fileService.saveFileToDisk(userContext.getUsername(), multipartFile);
        template.setPhotoSrc2(photoSrc);
        templateRepository.save(template);
        return templateConverter.toDTO(template);
    }

    public TemplateDTO addPicture3(UserContext userContext, Integer businessId, String name, MultipartFile multipartFile) {
        Template template = findTemplate(userContext, businessId, name);
        String photoSrc = fileService.saveFileToDisk(userContext.getUsername(), multipartFile);
        template.setPhotoSrc3(photoSrc);
        templateRepository.save(template);
        return templateConverter.toDTO(template);
    }

    public TemplateDTO addText1(UserContext userContext, Integer businessId, String name, TemplateTextDTO templateTextDTO) {
        Template template = findTemplate(userContext, businessId, name);
        template.setText1(templateTextDTO.getText());
        templateRepository.save(template);
        return templateConverter.toDTO(template);
    }

    public TemplateDTO addText2(UserContext userContext, Integer businessId, String name, TemplateTextDTO templateTextDTO) {
        Template template = findTemplate(userContext, businessId, name);
        template.setText2(templateTextDTO.getText());
        templateRepository.save(template);
        return templateConverter.toDTO(template);
    }

    public TemplateDTO addText3(UserContext userContext, Integer businessId, String name, TemplateTextDTO templateTextDTO) {
        Template template = findTemplate(userContext, businessId, name);
        template.setText3(templateTextDTO.getText());
        templateRepository.save(template);
        return templateConverter.toDTO(template);
    }

    public void selectTemplate(UserContext userContext, Integer businessId, String name){
        Template template = findTemplate(userContext, businessId, name);
        template.setSelected(true);
        List<Template> templates = templateRepository.findByBusinessId(businessId);
        templates.forEach(temp -> {
            temp.setSelected(true);
            templateRepository.save(temp);
        });
        templateRepository.save(template);
    }

    public void saveTemplate(UserContext userContext, Integer businessId, String name, String text1, String text2){

    }


    private Template findTemplate(UserContext userContext, Integer businessId, String name) {
        Business business = businessRepository.findOne(businessId);
//        Business business = businessRepository.findByIdAndAccountId(businessId, userContext.getId())
//                .orElseThrow(() -> new EntityNotFoundException("Business not found by id:" + businessId));

        Template template = templateRepository.findByBusinessIdAndName(business.getId(), name);
        if (template == null) {
            template = new Template();
            template.setName(name);
            template.setBusiness(business);
            templateRepository.save(template);
        }
        return template;
    }
}
