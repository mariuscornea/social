package com.social.template;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class TemplateTextDTO {

    @NotEmpty
    private String text;
}
