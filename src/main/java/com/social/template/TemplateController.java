package com.social.template;

import com.social.ResponseWithMessage;
import com.social.core.exception.FieldsException;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/auth")
public class TemplateController {

    @Autowired
    private TemplateService templateService;

    @RequestMapping(value = "/businesses/{businessId}/template/{templateName}", method = RequestMethod.GET)
    public TemplateDTO getTemplateByName(@AuthenticationPrincipal UserContext userContext,
                                         @PathVariable("businessId") Integer businessId,
                                         @PathVariable("templateName") String templateName) {
        return templateService.getTemplateByName(userContext, businessId, templateName);
    }

    @RequestMapping(value = "/businesses/{businessId}/template/{templateName}/picture1", method = RequestMethod.POST)
    public ResponseWithMessage<TemplateDTO> addTemplatePicture1(@AuthenticationPrincipal UserContext userContext,
                                                                @PathVariable("templateName") String templateName,
                                                                @PathVariable("businessId") Integer businessId,
                                                                @RequestParam(name = "file") final MultipartFile multipartFile) {
        return new ResponseWithMessage(templateService.addPicture1(userContext, businessId, templateName, multipartFile), "Upload successful");
    }

    @RequestMapping(value = "/businesses/{businessId}/template/{templateName}/picture2", method = RequestMethod.POST)
    public ResponseWithMessage<TemplateDTO> addTemplatePicture2(@AuthenticationPrincipal UserContext userContext,
                                                                @PathVariable("businessId") Integer businessId,
                                                                @PathVariable("templateName") String templateName,
                                                                @RequestParam(name = "file") final MultipartFile multipartFile) {
        return new ResponseWithMessage(templateService.addPicture2(userContext, businessId, templateName, multipartFile), "Upload successful");
    }

    @RequestMapping(value = "/businesses/{businessId}/template/{templateName}/picture3", method = RequestMethod.POST)
    public ResponseWithMessage<TemplateDTO> addTemplatePicture3(@AuthenticationPrincipal UserContext userContext,
                                                                @PathVariable("businessId") Integer businessId,
                                                                @PathVariable("templateName") String templateName,
                                                                @RequestParam(name = "file") final MultipartFile multipartFile) {
        return new ResponseWithMessage(templateService.addPicture3(userContext, businessId, templateName, multipartFile), "Upload successful");
    }

    @RequestMapping(value = "/businesses/{businessId}/template/{templateName}/text1", method = RequestMethod.POST)
    public ResponseWithMessage<TemplateDTO> addTemplateText1(@AuthenticationPrincipal UserContext userContext,
                                                             @PathVariable("businessId") Integer businessId,
                                                             @PathVariable("templateName") String templateName,
                                                             @Valid @RequestBody TemplateTextDTO templateTextDTO,
                                                             BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Template fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage(templateService.addText1(userContext, businessId, templateName, templateTextDTO), "Success message");
    }

    @RequestMapping(value = "/businesses/{businessId}/template/{templateName}/text2", method = RequestMethod.POST)
    public ResponseWithMessage<TemplateDTO> addTemplateText2(@AuthenticationPrincipal UserContext userContext,
                                                             @PathVariable("businessId") Integer businessId,
                                                             @PathVariable("templateName") String templateName,
                                                             @Valid @RequestBody TemplateTextDTO templateTextDTO,
                                                             BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Template fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage(templateService.addText2(userContext, businessId, templateName, templateTextDTO), "Success message");
    }

    @RequestMapping(value = "/businesses/{businessId}/template/{templateName}/text3", method = RequestMethod.POST)
    public ResponseWithMessage<TemplateDTO> addTemplateText3(@AuthenticationPrincipal UserContext userContext,
                                                             @PathVariable("templateName") String templateName,
                                                             @PathVariable("businessId") Integer businessId,
                                                             @Valid @RequestBody TemplateTextDTO templateTextDTO,
                                                             BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Template fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage(templateService.addText3(userContext, businessId, templateName, templateTextDTO), "Success message");
    }

    @RequestMapping(value = "/businesses/{businessId}/template/{templateName}/selected", method = RequestMethod.POST)
    public ResponseWithMessage<Void> selectTemplate(@AuthenticationPrincipal UserContext userContext,
                                                             @PathVariable("templateName") String templateName,
                                                             @PathVariable("businessId") Integer businessId) {
        templateService.selectTemplate(userContext, businessId, templateName);
        return new ResponseWithMessage("Success message");
    }

    @RequestMapping(value = "/businesses/{businessId}/template/{templateName}", method = RequestMethod.POST)
    public ResponseWithMessage<TemplateDTO> saveTemplate(@AuthenticationPrincipal UserContext userContext,
                                                                @PathVariable("templateName") String templateName,
                                                                @PathVariable("businessId") Integer businessId,
                                                                @RequestParam(name = "file") final MultipartFile multipartFile) {
        return null;
    }
}
