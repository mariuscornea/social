package com.social.template;

import org.springframework.stereotype.Component;

@Component
public class TemplateConverter {

    public TemplateDTO toDTO(Template template) {
        TemplateDTO templateDTO = new TemplateDTO();
        templateDTO.setName(template.getName());
        templateDTO.setPhotoSrc1(template.getPhotoSrc1());
        templateDTO.setPhotoSrc2(template.getPhotoSrc2());
        templateDTO.setPhotoSrc3(template.getPhotoSrc3());
        templateDTO.setText1(template.getText1());
        templateDTO.setText2(template.getText2());
        templateDTO.setText3(template.getText3());
        return templateDTO;
    }

}
