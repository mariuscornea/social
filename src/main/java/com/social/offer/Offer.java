package com.social.offer;

import com.social.account.Account;
import com.social.category.Category;
import com.social.util.enums.Currency;
import com.social.util.enums.OfferType;
import com.vividsolutions.jts.geom.Geometry;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
public class Offer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(value = AccessType.PROPERTY)
    private Integer id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Account account;

    @Column(nullable = false)
    private String title;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Category category;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private OfferType offerType;

    @Column(nullable = false)
    @Digits(integer=10, fraction=2)
    private BigDecimal budgetLowLimit;

    @Column(nullable = false)
    @Digits(integer=10, fraction=2)
    private BigDecimal budgetHighLimit;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Currency currency;

    @Column (nullable = false, columnDefinition = "geometry(Point,4326)")
    private Geometry geometry;

    @Column(nullable = false)
    private Double latitude;

    @Column(nullable = false)
    private Double longitude;

    @Column(nullable = false)
    private String description;

    @Column
    private LocalDateTime publicationDate = LocalDateTime.now();
}
