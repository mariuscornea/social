package com.social.offer.dto;

import com.social.util.enums.Currency;
import com.social.util.enums.OfferType;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class OfferDTO {

    private Integer id;
    private String categoryName;
    private String title;
    private OfferType offerType;
    private BigDecimal budgetLowLimit;
    private BigDecimal budgetHighLimit;
    private Currency currency;
    private Double latitude;
    private Double longitude;
    private String description;
}
