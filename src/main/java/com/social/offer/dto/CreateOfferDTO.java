package com.social.offer.dto;

import com.social.util.enums.Currency;
import com.social.util.enums.OfferType;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class CreateOfferDTO {

    @NotNull
    private String categoryName;

    @NotNull
    private String title;

    @NotNull
    private OfferType offerType;

    @Digits(integer=10, fraction=2)
    private BigDecimal budgetLowLimit;

    @Digits(integer=10, fraction=2)
    private BigDecimal budgetHighLimit;

    @NotNull
    private Currency currency;

    @NotNull
    private Double latitude;

    @NotNull
    private Double longitude;

    @NotNull
    private String description;

}
