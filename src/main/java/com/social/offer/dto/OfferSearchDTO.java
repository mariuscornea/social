package com.social.offer.dto;

import com.social.util.enums.OfferType;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class OfferSearchDTO {

    private Integer id;
    private String categoryName;
    private String title;
    private OfferType offerType;
    private BigDecimal budgetLowLimit;
    private BigDecimal budgetHighLimit;
    private String currency;
    private Double latitude;
    private Double longitude;
    private String description;
    private Integer daysLeft;
    private LocalDate creationDate;
}
