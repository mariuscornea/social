package com.social.offer;

import com.social.account.AccountRepository;
import com.social.category.Category;
import com.social.category.CategoryRepository;
import com.social.core.exception.EntityNotFoundException;
import com.social.file.FileService;
import com.social.offer.dto.CreateOfferDTO;
import com.social.offer.dto.OfferDTO;
import com.social.offer.dto.OfferSearchDTO;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class OfferService {

    @Autowired
    private OfferRepository offerRepository;

    @Autowired
    private OfferConverter offerConverter;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private FileService fileService;

    public OfferDTO findById(Integer offerId) {
        return offerConverter.toDTO(offerRepository.findOne(offerId));
    }

    public List<OfferDTO> findAllOffers(UserContext userContext) {
        return offerConverter.toDTOList(offerRepository.findByAccountIdOrderByIdDesc(userContext.getId()));
    }

    public OfferDTO createOffer(UserContext userContext, CreateOfferDTO createOfferDTO) {
        Category category = categoryRepository.findByName(createOfferDTO.getCategoryName())
                .orElseThrow(() -> new EntityNotFoundException("Category not found by name:" + createOfferDTO.getCategoryName()));

        Offer offer = offerConverter.toEntity(createOfferDTO);
        offer.setAccount(accountRepository.getOne(userContext.getId()));
        offer.setCategory(category);
        offer.setPublicationDate(LocalDateTime.now());

        offerRepository.save(offer);

        return offerConverter.toDTO(offer);
    }

    public OfferDTO editOffer(UserContext userContext, Integer offerId, CreateOfferDTO createOfferDTO) {
        Category category = categoryRepository.findByName(createOfferDTO.getCategoryName())
                .orElseThrow(() -> new EntityNotFoundException("Category not found by name:" + createOfferDTO.getCategoryName()));

        Offer offer = offerRepository.findByIdAndAccountId(offerId, userContext.getId())
                .orElseThrow(() -> new EntityNotFoundException("Offer not found by id:" + offerId));
        offerConverter.toEntity(offer, createOfferDTO);
        offer.setCategory(category);

        offerRepository.save(offer);

        return offerConverter.toDTO(offer);
    }

    public List<OfferSearchDTO> searchOffers(Map<String, String> parameters){
        return offerRepository.searchOffers(parameters);
    }
}
