package com.social.offer;

import com.social.offer.dto.CreateOfferDTO;
import com.social.offer.dto.OfferDTO;
import com.social.util.GeomUtil;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OfferConverter {

    public Offer toEntity(CreateOfferDTO createOfferDTO) {
        Offer offer = new Offer();
        return toEntity(offer, createOfferDTO);
    }

    public Offer toEntity(Offer offer, CreateOfferDTO createOfferDTO) {
        offer.setTitle(createOfferDTO.getTitle());
        offer.setOfferType(createOfferDTO.getOfferType());
        offer.setBudgetLowLimit(createOfferDTO.getBudgetLowLimit());
        offer.setBudgetHighLimit(createOfferDTO.getBudgetHighLimit());
        offer.setCurrency(createOfferDTO.getCurrency());
        offer.setLatitude(createOfferDTO.getLatitude());
        offer.setLongitude(createOfferDTO.getLongitude());
        offer.setGeometry(GeomUtil.latLongToGeom(createOfferDTO.getLatitude(), createOfferDTO.getLongitude()));
        offer.setDescription(createOfferDTO.getDescription());

        return offer;
    }

    public OfferDTO toDTO(Offer offer) {
        OfferDTO offerDTO = new OfferDTO();
        offerDTO.setId(offer.getId());
        offerDTO.setCategoryName(offer.getCategory().getName());
        offerDTO.setTitle(offer.getTitle());
        offerDTO.setOfferType(offer.getOfferType());
        offerDTO.setBudgetLowLimit(offer.getBudgetLowLimit());
        offerDTO.setBudgetHighLimit(offer.getBudgetHighLimit());
        offerDTO.setCurrency(offer.getCurrency());
        offerDTO.setLatitude(offer.getLatitude());
        offerDTO.setLongitude(offer.getLongitude());
        offerDTO.setDescription(offer.getDescription());
        return offerDTO;
    }

    public List<OfferDTO> toDTOList(List<Offer> offers) {
        return offers.stream().map(this::toDTO).collect(Collectors.toList());
    }

}
