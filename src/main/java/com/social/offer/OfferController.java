package com.social.offer;

import com.social.ResponseWithMessage;
import com.social.core.exception.FieldsException;
import com.social.offer.dto.CreateOfferDTO;
import com.social.offer.dto.OfferDTO;
import com.social.offer.dto.OfferSearchDTO;
import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/api")
public class OfferController {

    @Autowired
    private OfferService offerService;

    @RequestMapping(value = "/auth/offers", method = RequestMethod.POST)
    public ResponseWithMessage<OfferDTO> createOffer(@AuthenticationPrincipal UserContext userContext,
                                                     @Valid @RequestBody CreateOfferDTO createOfferDTO,
                                                     BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Offer fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage(offerService.createOffer(userContext, createOfferDTO), "Success message");
    }

    @RequestMapping(value = "/auth/offers/{offerId}", method = RequestMethod.PUT)
    public ResponseWithMessage<OfferDTO> editProject(@AuthenticationPrincipal UserContext userContext,
                                                     @PathVariable("offerId") Integer offerId,
                                                     @Valid @RequestBody CreateOfferDTO createOfferDTO,
                                                     BindingResult result) {
        if (result.hasErrors()) {
            throw new FieldsException("Offer fields are incorrect", "msg.key", result);
        }
        return new ResponseWithMessage(offerService.editOffer(userContext, offerId, createOfferDTO), "Success message");
    }

    @RequestMapping(value = "/auth/offers", method = RequestMethod.GET)
    public List<OfferDTO> getAllForUser(@AuthenticationPrincipal UserContext userContext) {
        return offerService.findAllOffers(userContext);
    }

    @RequestMapping(value = "/auth/offers/{offersId}", method = RequestMethod.GET)
    public OfferDTO getById(@PathVariable("offersId") Integer offersId) {
        return offerService.findById(offersId);
    }

    @RequestMapping(value = "/offers/search", method = RequestMethod.GET)
    public List<OfferSearchDTO> search(@RequestParam Map<String, String> parameters) {
        parameters.values().removeIf(x -> x.equals("null"));
        return offerService.searchOffers(parameters);
    }
}
