package com.social.offer.repo;

import com.social.core.repo.BaseRepository;
import com.social.core.sql.SQLQuery;
import com.social.core.sql.SQLQueryBuilder;
import com.social.offer.dto.OfferSearchDTO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class OfferRepositoryImpl extends BaseRepository implements OfferRepositoryCustom{

    public static final OfferSearchRowMapper OFFER_SEARCH_ROW_MAPPER = new OfferSearchRowMapper();

    @Override
    public List<OfferSearchDTO> searchOffers(Map<String, String> parameters) {
        SQLQueryBuilder queryBuilder = new SQLQueryBuilder()
                .select()
                .column("o.id", "id")
                .column("o.title", "title")
                .column("c.name", "categoryName")
                .column("o.budget_low_limit", "budgetLowLimit")
                .column("o.budget_high_limit", "budgetHighLimit")
                .column("o.currency", "currency")
                .column("o.latitude", "latitude")
                .column("o.longitude", "longitude")
                .column("o.description", "description")
                .column("o.creation_date", "creationDate")
                .from("offer", "o")
                .innerJoin("category", "c")
                .append(" ON o.category_id = c.id");
        if (parameters.containsKey("category")) {
            queryBuilder.append(" AND c.name = ?", parameters.get("category"));
        }
        queryBuilder.where().append("1 = 1");
        if (parameters.containsKey("name")) {
            queryBuilder.append(" AND c.title ILIKE ?", "%"+ parameters.get("name") + "%");
        }
        if(parameters.containsKey("longitude") && parameters.containsKey("latitude")){
            queryBuilder.append(" AND ST_DWithin(o.geometry, ST_GeographyFromText('SRID=4326; POINT(' || ? || ' ' || ? || ')'), 1000)", parameters.get("longitude"), parameters.get("latitude"));
        }

        SQLQuery query = queryBuilder.build();

        return jdbcTemplate.query(query.getQuery(), query.getParams(), OFFER_SEARCH_ROW_MAPPER);
    }
}
