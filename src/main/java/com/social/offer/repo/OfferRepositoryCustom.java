package com.social.offer.repo;

import com.social.offer.dto.OfferSearchDTO;

import java.util.List;
import java.util.Map;

public interface OfferRepositoryCustom {

    List<OfferSearchDTO> searchOffers(Map<String, String> parameters);
}
