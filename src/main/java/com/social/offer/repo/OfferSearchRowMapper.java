package com.social.offer.repo;

import com.social.offer.dto.OfferSearchDTO;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;


public class OfferSearchRowMapper implements RowMapper<OfferSearchDTO> {

    @Nullable
    @Override
    public OfferSearchDTO mapRow(ResultSet resultSet, int i) throws SQLException {
        OfferSearchDTO offerSearchDTO = new OfferSearchDTO();
        offerSearchDTO.setId(resultSet.getInt("id"));
        offerSearchDTO.setCategoryName(resultSet.getString("categoryName"));
        offerSearchDTO.setTitle(resultSet.getString("title"));
        offerSearchDTO.setBudgetLowLimit(resultSet.getBigDecimal("budgetLowLimit"));
        offerSearchDTO.setBudgetHighLimit(resultSet.getBigDecimal("budgetHighLimit"));
        offerSearchDTO.setCurrency(resultSet.getString("currency"));
        offerSearchDTO.setLatitude(resultSet.getDouble("latitude"));
        offerSearchDTO.setLongitude(resultSet.getDouble("longitude"));
        offerSearchDTO.setDescription(resultSet.getString("description"));
        offerSearchDTO.setCreationDate(resultSet.getTimestamp("creationDate").toLocalDateTime().toLocalDate());

        return offerSearchDTO;
    }
}
