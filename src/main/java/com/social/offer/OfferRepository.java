package com.social.offer;

import com.social.core.repo.BaseJpaRepository;
import com.social.offer.repo.OfferRepositoryCustom;

import java.util.List;
import java.util.Optional;

public interface OfferRepository extends BaseJpaRepository<Offer, Integer>, OfferRepositoryCustom{

    List<Offer> findByAccountIdOrderByIdDesc(Integer accountId);

    Optional<Offer> findByIdAndAccountId(Integer offerId, Integer accountId);

    List<Offer> findTop10ByOrderByPublicationDateDesc();
}
