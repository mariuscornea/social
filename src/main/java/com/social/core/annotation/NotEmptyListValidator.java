package com.social.core.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class NotEmptyListValidator implements ConstraintValidator<NotEmptyList, List<String>> {

    @Override
    public void initialize(NotEmptyList constraintAnnotation) {
    }

    @Override
    public boolean isValid(List<String> objects, ConstraintValidatorContext context) {
        if (objects == null)
            return false;
        else if (objects.isEmpty()) {
            return false;
        }
        return true;
    }
}
