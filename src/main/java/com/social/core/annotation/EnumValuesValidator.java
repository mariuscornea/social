package com.social.core.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class EnumValuesValidator implements ConstraintValidator<EnumValues, String> {
    private List<String> values = new ArrayList<>();

    @Override
    public void initialize(EnumValues enumValue) {
        values = new ArrayList<String>();
        Enum<?> [] enumConstants = enumValue.enumClass().getEnumConstants();
        for (Enum<?> enumConstant : enumConstants) {
            values.add(enumConstant.toString());
        }
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            return true;
        }
        return values.contains(value);
    }
}
