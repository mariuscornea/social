package com.social.core.exception;


import com.social.core.exception.core.LocalizableRuntimeException;

public class ConflictException extends LocalizableRuntimeException {

    private static final long serialVersionUID = 54447954588764268L;

    private Class entityClass;

    public ConflictException(String message) {
        super(message);
    }

    public ConflictException(String message, String messageKey, Object... messageArguments) {
        super(message, messageKey, messageArguments);
    }

    public ConflictException(Class entityClass, String message) {
        super(message);
        this.entityClass = entityClass;
    }

    public Class getEntityClass() {
        return entityClass;
    }
}