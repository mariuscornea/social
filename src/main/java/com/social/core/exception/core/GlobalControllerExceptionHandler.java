package com.social.core.exception.core;

import com.social.core.exception.BadCredentialsException;
import com.social.core.exception.ConflictException;
import com.social.core.exception.EntityNotFoundException;
import com.social.core.exception.FieldsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class GlobalControllerExceptionHandler {
    private final Logger logger = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

    @Autowired
    private ErrorInfoFactory errorInfoFactory;

    @ExceptionHandler(value = {BadCredentialsException.class})
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ErrorInfo handleBadCredentialsException(BadCredentialsException e) {
        return ErrorInfo.builder().status(HttpStatus.UNAUTHORIZED.value()).messageKey("bad.credential.message")
                .message("Bad Credentials Message").developerMessage(e.getMessage()).build();
    }

    @ExceptionHandler(value = {ConflictException.class})
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorInfo handleConflictException(ConflictException e) {
        return ErrorInfo.builder().status(HttpStatus.CONFLICT.value()).messageKey("conflict.message")
                .message("Conflict Message").developerMessage(e.getMessage()).build();
    }

    @ExceptionHandler(value = {EntityNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorInfo handleConflictException(EntityNotFoundException e) {
        return ErrorInfo.builder().status(HttpStatus.NOT_FOUND.value()).messageKey("not.found.message")
                .message("Not Found Message").developerMessage(e.getMessage()).build();
    }

    @ExceptionHandler(value = {FieldsException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorInfo handleFieldsException(FieldsException e) {
        return ErrorInfoFactory.buildBadRequestErrorInfo("Company fields are incorrect", "msg.key", e.getResult());
    }

}
