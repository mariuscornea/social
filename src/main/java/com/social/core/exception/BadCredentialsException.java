package com.social.core.exception;


import com.social.core.exception.core.LocalizableRuntimeException;

public class BadCredentialsException extends LocalizableRuntimeException {

    private Class entityClass;

    public BadCredentialsException(String message) {
        super(message);
    }

    public BadCredentialsException(String message, String messageKey, Object... messageArguments) {
        super(message, messageKey, messageArguments);
    }

    public BadCredentialsException(Class entityClass, String message) {
        super(message);
        this.entityClass = entityClass;
    }

    public Class getEntityClass() {
        return entityClass;
    }
}
