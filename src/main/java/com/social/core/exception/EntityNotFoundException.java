/**
 *
 */
package com.social.core.exception;


import com.social.core.exception.core.LocalizableRuntimeException;

public class EntityNotFoundException extends LocalizableRuntimeException {
	private static final long serialVersionUID = -6237901329312044859L;

	private Class entityClass;

	public EntityNotFoundException(String message) {
		super(message);
	}

	public EntityNotFoundException(String message, String messageKey, Object... messageArguments) {
		super(message, messageKey, messageArguments);
	}

	public EntityNotFoundException(Class entityClass, String message) {
		super(message);
		this.entityClass = entityClass;
	}

	public Class getEntityClass() {
		return entityClass;
	}

}
