package com.social.core.exception;


import com.social.core.exception.core.LocalizableRuntimeException;

public class ServerOperationException extends LocalizableRuntimeException {

    private static final long serialVersionUID = 8647726307266361229L;

    private Class entityClass;

    public ServerOperationException(String message) {
        super(message);
    }

    public ServerOperationException(String message, String messageKey, Object... messageArguments) {
        super(message, messageKey, messageArguments);
    }

    public ServerOperationException(Class entityClass, String message) {
        super(message);
        this.entityClass = entityClass;
    }

    public Class getEntityClass() {
        return entityClass;
    }
}
