package com.social.core.exception;

import com.social.core.exception.core.LocalizableRuntimeException;
import org.springframework.validation.BindingResult;

public class FieldsException extends LocalizableRuntimeException {

    private static final long serialVersionUID = 54447954588764268L;

    private Class entityClass;
    private BindingResult result;

    public FieldsException(String message, BindingResult result) {
        super(message);
        this.result = result;
    }

    public FieldsException(String message, String messageKey, BindingResult result, Object... messageArguments) {
        super(message, messageKey, messageArguments);
        this.result = result;
    }

    public FieldsException(Class entityClass, String message) {
        super(message);
        this.entityClass = entityClass;
    }

    public Class getEntityClass() {
        return entityClass;
    }

    public BindingResult getResult() {
        return result;
    }
}