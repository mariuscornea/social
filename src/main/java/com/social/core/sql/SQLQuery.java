package com.social.core.sql;

public class SQLQuery {

    private final String query;
    private final Object[] params;

    public SQLQuery(String query, Object[] params) {
        this.query = query;
        this.params = params;
    }

    public String getQuery() {
        return query;
    }

    public Object[] getParams() {
        return params;
    }

}
