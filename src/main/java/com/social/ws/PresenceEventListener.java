package com.social.ws;

import com.social.security.auth.JwtAuthenticationToken;
import com.social.security.model.UserContext;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class PresenceEventListener {

    private final SimpMessagingTemplate messagingTemplate;
    //sessionId and username
    private final ConcurrentMap<String, String> sessionMap;
    private final ConcurrentMap<String, ConcurrentMap<String, String>> wbSocketIdMap;

    public PresenceEventListener(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
        this.sessionMap = new ConcurrentHashMap<>();
        this.wbSocketIdMap = new ConcurrentHashMap<>();
    }

    @EventListener
    private void handleSessionConnected(SessionConnectEvent event) {
        String username = ((UserContext)((JwtAuthenticationToken) event.getUser()).getPrincipal()).getUsername();
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
        System.out.println("[Connected] " + sha.getSessionId() + " User: " + event.getUser().getName());
        sessionMap.putIfAbsent(sha.getSessionId(), username);
        wbSocketIdMap.putIfAbsent(username, new ConcurrentHashMap<>());
    }

    @EventListener
    private void handleSessionDisconnect(SessionDisconnectEvent event) {
        String username = ((UserContext)((JwtAuthenticationToken) event.getUser()).getPrincipal()).getUsername();
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
        System.out.println("Disconnected " + event.getSessionId() + " Session attribute: " + sha.getSessionAttributes());
        sessionMap.remove(sha.getSessionId());
        wbSocketIdMap.remove(username);
        System.out.println(sessionMap);
    }

    public ConcurrentMap<String, String> getSessionMap() {
        return sessionMap;
    }

    public ConcurrentMap<String, ConcurrentMap<String, String>> getWbSocketIdMap() {
        return wbSocketIdMap;
    }
}
