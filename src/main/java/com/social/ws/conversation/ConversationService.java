package com.social.ws.conversation;

import com.social.account.Account;
import com.social.account.AccountRepository;
import com.social.security.model.UserContext;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ConversationService {

    @Autowired
    private ConversationRepository conversationRepository;

    @Autowired
    private ConversationConverter conversationConverter;

    @Autowired
    private AccountRepository accountRepository;

    public ConversationDTO getConversationInfo(UserContext userContext, Integer accountId) {
        Account account = accountRepository.findOne(accountId);
        Conversation conversation = conversationRepository.findConversationForAccounts(userContext.getUsername(), account.getEmail());
        if(conversation == null){
            conversation = new Conversation();
            conversation.setAccount1(userContext.getUsername());
            conversation.setAccount2(account.getEmail());
            conversation.setWbSocketId(RandomStringUtils.randomAlphabetic(10));
            conversationRepository.save(conversation);
        }
        return conversationConverter.toDTO(conversation);
    }
}
