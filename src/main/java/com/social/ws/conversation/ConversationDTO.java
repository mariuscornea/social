package com.social.ws.conversation;

import lombok.Data;

@Data
public class ConversationDTO {

    private String wbSocketId;
}
