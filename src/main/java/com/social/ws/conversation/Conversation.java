package com.social.ws.conversation;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Conversation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.PROPERTY)
    private Integer id;

    @Column(nullable = false)
    private String wbSocketId;

    //username
    private String account1;

    //username
    private String account2;

}
