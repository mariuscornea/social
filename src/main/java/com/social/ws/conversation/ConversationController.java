package com.social.ws.conversation;


import com.social.security.model.UserContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class ConversationController {


    @Autowired
    private ConversationService conversationService;

    @RequestMapping(value = "/auth/chanel-info/{accountId}", method = RequestMethod.GET)
    public void getChanelInfo(@AuthenticationPrincipal UserContext userContext,
                              @PathVariable("accountId") Integer accountId) {
        conversationService.getConversationInfo(userContext, accountId);
    }
}
