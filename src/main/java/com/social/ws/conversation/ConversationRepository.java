package com.social.ws.conversation;

import com.social.core.repo.BaseJpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ConversationRepository extends BaseJpaRepository<Conversation, Integer> {

    @Query("SELECT c FROM Conversation c WHERE (c.account1 = :account1 AND c.account2 = :account2) OR (c.account1 = :account2 AND c.account2 = :account1)")
    Conversation findConversationForAccounts(@Param("account1") String account1, @Param("account2") String account2);

    @Query("SELECT c FROM Conversation c WHERE (c.account1.id = :accountId AND c.wbSocketId = :wbSocketId) OR (c.account2.id = :accountId AND c.wbSocketId = :wbSocketId)")
    Conversation findAccountAndWbSocket(@Param("accountId") String accountId, @Param("wbSocketId") String wbSocketId);

    Conversation findByAccount1AndWbSocketId(String accountId, String wbSocketId);

    Conversation findByAccount2AndWbSocketId(String accountId, String wbSocketId);
}
