package com.social.ws.conversation;

import org.springframework.stereotype.Component;

@Component
public class ConversationConverter {

    public ConversationDTO toDTO(Conversation conversation){
        ConversationDTO conversationDTO = new ConversationDTO();
        conversationDTO.setWbSocketId(conversation.getWbSocketId());
        return conversationDTO;
    }
}
