package com.social.ws.chat;

import com.social.security.auth.JwtAuthenticationToken;
import com.social.security.model.UserContext;
import com.social.ws.PresenceEventListener;
import com.social.ws.conversation.ConversationRepository;
import com.social.ws.message.MessageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;

@RestController
public class ChatController {

    @Autowired
    private ChatService chatService;

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private ConversationRepository conversationRepository;

    @Autowired
    private PresenceEventListener presenceEventListener;

    @RequestMapping(value = "/chat", method = RequestMethod.GET)
    public void test(@AuthenticationPrincipal UserContext userContext) {
        System.out.println("Chat controller");
        System.out.println(userContext);
    }

    @SubscribeMapping("/conversation/{wbSocketId}")
    public List<MessageDTO> chatHistory(@DestinationVariable("wbSocketId") String wbSocketId, Principal principal) {
        String username = ((UserContext) ((JwtAuthenticationToken) principal).getPrincipal()).getUsername();
        chatService.subscribe(wbSocketId, (UserContext) ((JwtAuthenticationToken) principal).getPrincipal());
        return chatService.initChatHistory(username, presenceEventListener.getWbSocketIdMap().get(username).get(wbSocketId));
    }

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public void test() {
        System.out.println(presenceEventListener.getSessionMap());
    }

    @MessageMapping("/conversation/{wbSocketId}")
    public void sendMessage(@Payload MessageDTO messageDTO, @DestinationVariable("wbSocketId") String wbSocketId, Principal principal) {
        String username = ((UserContext) ((JwtAuthenticationToken) principal).getPrincipal()).getUsername();
        String receiverId = presenceEventListener.getWbSocketIdMap().get(username).get(wbSocketId);
        chatService.sendMessage(username, receiverId, messageDTO.getMessage());
        messagingTemplate.convertAndSend("/user/conversation/" + wbSocketId, messageDTO);
    }
}
