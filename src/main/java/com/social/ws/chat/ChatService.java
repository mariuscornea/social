package com.social.ws.chat;

import com.social.account.AccountRepository;
import com.social.security.model.UserContext;
import com.social.ws.PresenceEventListener;
import com.social.ws.conversation.Conversation;
import com.social.ws.conversation.ConversationRepository;
import com.social.ws.message.MessageConverter;
import com.social.ws.message.MessageDTO;
import com.social.ws.message.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.ConcurrentMap;

@Service
@Transactional
public class ChatService {

    @Autowired
    private PresenceEventListener presenceEventListener;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private MessageConverter messageConverter;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ConversationRepository conversationRepository;

    public void subscribe(String wbSocketId, UserContext userContext) {
        ConcurrentMap<String, ConcurrentMap<String, String>> wbSocketIdMap = presenceEventListener.getWbSocketIdMap();
        String username;
        Conversation conversation = conversationRepository.findByAccount1AndWbSocketId(userContext.getUsername(), wbSocketId);
        if (conversation != null) {
            username = conversation.getAccount2();
        } else {
            conversation = conversationRepository.findByAccount2AndWbSocketId(userContext.getUsername(), wbSocketId);
            username = conversation.getAccount1();
        }
        wbSocketIdMap.get(userContext.getUsername()).putIfAbsent(wbSocketId, username);
    }

    public void sendMessage(String senderId, String receiverId, String message){
        messageRepository.addMessage(senderId, receiverId, message, System.currentTimeMillis());
    }

    public List<MessageDTO> initChatHistory(String account1, String account2){
        return messageConverter.toDTOList(messageRepository.initHistory(account1, account2));
    }

}
