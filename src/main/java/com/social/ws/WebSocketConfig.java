package com.social.ws;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic", "/queue", "/user");
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/api/auth/chat")
                .setAllowedOrigins("http://localhost:4200", "https://localhost:4200")
                .setHandshakeHandler(new CustomHandshakeHandler())
                .withSockJS();
//                .setClientLibraryUrl("/wbsocket/sockjs.min.js");
        //.setHandshakeHandler(new CustomHandshakeHandler()).withSockJS();
    }


//    @EventListener
//    public void onSocketDisconnected(SessionDisconnectEvent event) {
//        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
//        System.out.println("Disconnected " + sha.getSessionId() + " Session attribute: " + sha.getSessionAttributes());
//    }
//
//    @EventListener
//    public void onSocketConnected(SessionConnectedEvent event) {
//        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
//        System.out.println("[Connected] " + sha.getSessionId() + " User: " + event.getUser().getName());
//    }
}