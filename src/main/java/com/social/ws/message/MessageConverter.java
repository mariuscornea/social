package com.social.ws.message;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class MessageConverter {

    public MessageDTO toDTO(Message message) {
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setSender(message.getSender());
        messageDTO.setReceiver(message.getReceiver());
        messageDTO.setMessage(message.getMessage());
        return messageDTO;
    }

    public List<MessageDTO> toDTOList(List<Message> messages){
        return messages.stream().map(this::toDTO).collect(Collectors.toList());
    }
}
