package com.social.ws.message;

import com.social.core.repo.BaseJpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageRepository extends BaseJpaRepository<Message, Integer> {

    @Modifying
    @Query(nativeQuery = true, value = "INSERT INTO message(sender, receiver, message, time) VALUES(:senderId, :receiverId, :message, :time)")
    void addMessage(@Param("senderId") String senderId, @Param("receiverId") String receiverId, @Param("message") String message, @Param("time") Long time);


    @Query(value = "SELECT * FROM message WHERE (sender = :account1 AND receiver = :account2) OR (sender = :account2 AND receiver = :account1)" +
            "ORDER BY time DESC OFFSET 0 FETCH NEXT 20 ROWS ONLY", nativeQuery = true)
    List<Message> initHistory(@Param("account1") String account1, @Param("account2") String account2);
}
